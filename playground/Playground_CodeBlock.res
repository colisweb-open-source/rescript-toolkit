module HighlightJs = {
  @module("highlight.js") @scope("default")
  external registerLanguage: (string, 'a) => unit = "registerLanguage"
  @module("highlight.js") @scope("default")
  external highlightAll: unit => unit = "highlightAll"

  type result = {value: string}
  @module("highlight.js") @scope("default")
  external highlight: (string, 'a) => result = "highlight"
}

@module("../../../playground/utils/rescript-highlight")
external rescriptModule: 'a = "default"

HighlightJs.registerLanguage("rescript", rescriptModule)

@react.component
let make = (~code) => {
  <div className="Code border rounded-lg mb-6 relative">
    <pre className="p-4">
      <code
        dangerouslySetInnerHTML={
          "__html": HighlightJs.highlight(code, {"language": "rescript"}).value,
        }
      />
    </pre>
    <button
      className="absolute right-0 top-0 bg-white rounded-tl rounded-br text-sm px-2 border-b border-l hover:bg-neutral-200"
      onClick={_ => Browser.Navigator.Clipboard.writeText(Obj.magic(code))}>
      {"Copy"->React.string}
    </button>
  </div>
}
