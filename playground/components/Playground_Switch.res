open Toolkit__Ui

@react.component
let make = () => {
  let (controlled, setControlled) = React.useState(() => true)

  <div>
    <h2> {"3 sizes : "->React.string} </h2>
    <div className="flex flex-col gap-2">
      <div>
        <strong> {"sm"->React.string} </strong>
        <Switch size=#sm />
      </div>
      <div>
        <strong> {"md"->React.string} </strong>
        <Switch size=#md />
      </div>
      <div>
        <strong> {"lg"->React.string} </strong>
        <Switch color={Black} size=#lg />
      </div>
    </div>
    <br />
    <Switch size=#md checked=true />
    <br />
    <Switch
      className="border p-1 rounded"
      checkedClassName="border-info-500"
      size=#lg
      label={<p> {"simple label"->React.string} </p>}
    />
    <hr />
    <h2> {"With title"->React.string} </h2>
    <div className="flex flex-col gap-2">
      <div>
        <strong> {"sm"->React.string} </strong>
        <Switch size=#sm displayTitle={true} />
      </div>
      <div>
        <strong> {"md"->React.string} </strong>
        <Switch size=#md displayTitle={true} />
      </div>
      <div>
        <strong> {"lg"->React.string} </strong>
        <Switch size=#lg displayTitle={true} />
      </div>
    </div>
    <hr />
    <h2> {"Controlled"->React.string} </h2>
    <div className="flex flex-row gap-4">
      <Switch size=#md checked={controlled} onChange={checked => setControlled(_ => checked)} />
      <button className="bg-gray-200 border p-1" onClick={_ => setControlled(v => !v)}>
        {"toggle"->React.string}
      </button>
      <button className="bg-gray-200 border p-1" onClick={_ => setControlled(_v => true)}>
        {"true"->React.string}
      </button>
      <button className="bg-gray-200 border p-1" onClick={_ => setControlled(_v => false)}>
        {"false"->React.string}
      </button>
    </div>
  </div>
}
