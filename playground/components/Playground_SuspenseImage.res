@react.component
let make = () => {
  <div>
    <React.Suspense fallback={<Toolkit.Ui.Spinner />}>
      <Toolkit__Ui_SuspenseImage src="https://placehold.co/600x400" />
    </React.Suspense>
    <Toolkit.Ui.ErrorBoundary fallbackRender={_ => <p> {"loading failed"->React.string} </p>}>
      <React.Suspense fallback={<Toolkit.Ui.Spinner />}>
        <Toolkit__Ui_SuspenseImage src="randomurinotexisting" />
      </React.Suspense>
    </Toolkit.Ui.ErrorBoundary>
  </div>
}
