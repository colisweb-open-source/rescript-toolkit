open Toolkit__Ui

@react.component
let make = () => {
  let (uncontrolledValue, setUncontrolledValue) = React.useState((): option<
    HeadlessUi.Listbox.selectOption<string>,
  > => None)

  let options: array<HeadlessUi.Listbox.selectOption<string>> = [
    {
      label: "Label1",
      value: "1",
    },
    {
      label: "Label2 pretty long",
      value: "2",
    },
    {
      label: "Label3 disabled",
      value: "3",
      disabled: true,
    },
  ]

  <div className="flex flex-col gap-6">
    <div>
      <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
        {"Listbox uncontrolled"->React.string}
      </Label>
      <Toolkit__Ui_SearchListbox
        placeholder="This is a placeholder"
        compare={ValueEquality}
        onChange={value => setUncontrolledValue(_ => Some(value))}
        options
      />
      <div className="mt-4">
        <p> {"selected value :"->React.string} </p>
        {uncontrolledValue->Option.mapWithDefault(React.null, value =>
          <p> {value.label->React.string} </p>
        )}
      </div>
    </div>
  </div>
}
