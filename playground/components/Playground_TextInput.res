open Toolkit__Ui

@react.component
let make = () =>
  <div className="flex flex-col gap-4">
    <div>
      <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
        {"Label"->React.string}
      </Label>
      <TextInput id="test" placeholder="Test" />
    </div>
    <div>
      <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
        {"Label"->React.string}
      </Label>
      <TextInput id="test" placeholder="Test" isInvalid=true />
    </div>
  </div>
