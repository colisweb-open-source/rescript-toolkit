@react.component
let make = () => {
  let colors: array<Toolkit__Ui_Button.color> = [
    #primary,
    #black,
    #white,
    #gray,
    #success,
    #info,
    #danger,
    #warning,
    #neutral,
    #neutralLight,
    #dynamic,
  ]

  let sizes = Js.Dict.fromArray([("xs", #xs), ("sm", #sm), ("md", #md), ("lg", #lg)])
  let variants = Js.Dict.fromArray([
    ("default", #default),
    ("text", #text),
    ("link", #link),
    ("outline", #outline),
    ("pill", #pill),
  ])
  let (primaryCtaColor, setPrimaryCtaColor) = React.useState(() => "#131626")
  let (primaryCtaTextColor, setPrimaryCtaTextColor) = React.useState(() => "#fff")

  <div>
    <style>
      {Obj.magic(
        `
      :root {
        --primaryCta: ${primaryCtaColor};
        --primaryCtaText: ${primaryCtaTextColor};
        --primaryText: #131626;
      }
      `,
      )}
    </style>
    {colors
    ->Array.mapWithIndex((i, color) =>
      <div
        key={i->Int.toString ++ (color :> string)}
        className="flex flex-col gap-4 first:border-0 border-t pt-4 first:pt-0 border-neutral-500">
        <h2> {(color :> string)->React.string} </h2>
        {color == #dynamic
          ? <section className={"flex flex-row gap-4"}>
              <div>
                <Toolkit__Ui.Label htmlFor="textColor">
                  {"Couleur du texte"->React.string}
                </Toolkit__Ui.Label>
                <Toolkit__Ui.TextInput
                  id={"textColor"}
                  defaultValue={primaryCtaTextColor}
                  onChangeText={v => setPrimaryCtaTextColor(_ => v)}
                />
              </div>
              <div>
                <Toolkit__Ui.Label htmlFor="textCtaColor">
                  {"Couleur du CTA"->React.string}
                </Toolkit__Ui.Label>
                <Toolkit__Ui.TextInput
                  id={"textCtaColor"}
                  defaultValue={primaryCtaColor}
                  onChangeText={v => setPrimaryCtaColor(_ => v)}
                />
              </div>
            </section>
          : React.null}
        <div className="flex flex-row flex-wrap gap-8">
          {variants
          ->Js.Dict.entries
          ->Array.map(((variantText, variant)) => {
            <div key={i->Int.toString ++ (color :> string) ++ variantText ++ (variant :> string)}>
              <h4> {variantText->React.string} </h4>
              <div className="flex flex-col">
                {[false, true]
                ->Array.mapWithIndex(
                  (j, disabled) =>
                    <div
                      key={i->Int.toString ++
                      (color :> string) ++
                      variantText ++
                      (variant :> string) ++
                      j->Int.toString}
                      className="flex items-center gap-1 mb-2">
                      {sizes
                      ->Js.Dict.entries
                      ->Array.mapWithIndex(
                        (k, (sizeText, size)) =>
                          <div
                            key={(i + j + k)->Int.toString ++ ((color :> string) ++ sizeText)}
                            className="flex flex-col justify-center items-center">
                            <Toolkit__Ui_Button variant color size disabled>
                              {"hello"->React.string}
                            </Toolkit__Ui_Button>
                            <span className="text-xxs"> {sizeText->React.string} </span>
                          </div>,
                      )
                      ->React.array}
                    </div>,
                )
                ->React.array}
                <div className="flex items-center gap-1 mb-2">
                  {sizes
                  ->Js.Dict.entries
                  ->Array.mapWithIndex(
                    (k, (sizeText, size)) =>
                      <div
                        key={(i + k)->Int.toString ++ "icon" ++ ((color :> string) ++ sizeText)}
                        className="flex flex-col justify-center items-center">
                        <Toolkit__Ui_Button
                          leftIcon=module(ReactIcons.FaCheck)
                          variant
                          color
                          size
                          confirmationLabel={"Confirmation label on click"}
                          onClick={_ => Js.log("test")}>
                          {"hello"->React.string}
                        </Toolkit__Ui_Button>
                        <span className="text-xxs"> {sizeText->React.string} </span>
                      </div>,
                  )
                  ->React.array}
                </div>
              </div>
            </div>
          })
          ->React.array}
        </div>
      </div>
    )
    ->React.array}
  </div>
}
