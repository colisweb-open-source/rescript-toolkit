@react.component
let make = () => {
  let options1: array<Toolkit__Ui_MultiSelect.item> = [
    {
      itemLabel: <p className="flex flex-row text-danger-500">
        <ReactIcons.FaAngleDown />
        {"test"->React.string}
      </p>,
      label: "Test",
      value: "test",
    },
    {
      itemLabel: <p> {"test2"->React.string} </p>,
      label: "Test2",
      value: "test2",
    },
    {
      itemLabel: <p> {"test3"->React.string} </p>,
      label: "Test3",
      value: "test3",
    },
    {
      itemLabel: <p> {"test4"->React.string} </p>,
      label: "Test4",
      value: "test4",
    },
  ]
  let options2: array<Toolkit__Ui_MultiSelect.item> = [
    {
      itemLabel: <p> {"test4"->React.string} </p>,
      label: "Test4",
      value: "test4",
    },
  ]

  let tmp = Toolkit__Hooks.useDisclosure()

  let options = tmp.isOpen ? options2 : options1

  <div className="flex flex-col gap-8 items-start">
    <Toolkit__Ui_Button onClick={_ => tmp.toggle()}>
      {"Change options"->React.string}
    </Toolkit__Ui_Button>
    <Toolkit__Ui_MultiSelect
      searchPlaceholder="Filter"
      buttonClassName={"w-48"}
      dropdownClassName="w-60"
      placeholder={<span> {"Select me"->React.string} </span>}
      onChange={v => Js.log(v)}
      options
    />
    <div>
      <h3 className="mb-2"> {"Preselected options"->React.string} </h3>
      <Toolkit__Ui_MultiSelect
        searchPlaceholder="Filter"
        buttonClassName={"w-48"}
        dropdownClassName="w-60"
        defaultValue={options}
        placeholder={<span> {"Select me"->React.string} </span>}
        onChange={v => Js.log(v)}
        options
      />
    </div>
    <div>
      <h3 className="mb-2"> {"Search"->React.string} </h3>
      <p className="mb-1">
        {"Search input is displayed when there are more than 5 options"->React.string}
      </p>
      <Toolkit__Ui_MultiSelect
        searchPlaceholder="Filter"
        buttonClassName={"w-48"}
        dropdownClassName="w-60 max-h-[300px] overflow-auto"
        options={Array.makeBy(10, (i): Toolkit__Ui.MultiSelect.item => {
          {
            itemLabel: <p> {`label ${i->Int.toString}`->React.string} </p>,
            label: `label ${i->Int.toString}`,
            value: `label ${i->Int.toString}`,
          }
        })}
        placeholder={<span> {"Select me"->React.string} </span>}
        onChange={v => Js.log(v)}
      />
    </div>
    <div>
      <h3 className="mb-2"> {"Select only button"->React.string} </h3>
      <p className="mb-1">
        {"Select only button is displayed when the displaySelectOnlyOption prop is at true"->React.string}
      </p>
      <Toolkit__Ui_MultiSelect
        searchPlaceholder="Filter"
        buttonClassName={"w-80"}
        dropdownClassName="max-h-[300px] w-80 overflow-auto"
        displaySelectOnlyOption=true
        options={Array.makeBy(10, (i): Toolkit__Ui.MultiSelect.item => {
          {
            itemLabel: <p> {`label ${i->Int.toString}`->React.string} </p>,
            label: `label ${i->Int.toString}`,
            value: `label ${i->Int.toString}`,
          }
        })}
        placeholder={<span> {"Select me"->React.string} </span>}
        onChange={v => Js.log(v)}
      />
    </div>
  </div>
}
