@react.component
let make = () => {
  let colors: array<Toolkit__Ui.Tag.color> = [
    #black,
    #white,
    #gray,
    #primary,
    #info,
    #danger,
    #warning,
    #success,
    #neutral,
  ]

  let size: array<Toolkit__Ui.Tag.size> = [#xs, #sm, #md, #lg]
  let variants: array<Toolkit__Ui.Tag.variant> = [#plain, #outline]

  <div className="flex flex-col gap-4">
    {variants
    ->Array.map(variant => {
      <div>
        <h2> {(variant :> string)->React.string} </h2>
        <div className="flex flex-col gap-2">
          {colors
          ->Array.mapWithIndex((i, color) =>
            <div key={i->Int.toString ++ color->Obj.magic} className="flex items-center gap-4">
              <strong className="w-32"> {(color :> string)->React.string} </strong>
              {size
              ->Array.mapWithIndex(
                (j, size) =>
                  <div key={(i + j)->Int.toString ++ size->Obj.magic}>
                    <Toolkit__Ui_Tag variant color size>
                      {"content"->React.string}
                    </Toolkit__Ui_Tag>
                  </div>,
              )
              ->React.array}
            </div>
          )
          ->React.array}
        </div>
      </div>
    })
    ->React.array}
  </div>
}
