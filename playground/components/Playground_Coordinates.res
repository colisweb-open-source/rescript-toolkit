@react.component
let make = () => {
  <div className="flex gap-4">
    <div className="inline-block">
      <Toolkit__Ui_Coordinates coordinates={{latitude: 50.222, longitude: 20.33333}} />
    </div>
    <div className="inline-block">
      <Toolkit__Ui_Coordinates coordinates={{latitude: 50., longitude: 20.33333}} />
    </div>
    <div className="inline-block">
      <Toolkit__Ui_Coordinates coordinates={{latitude: 50., longitude: 20.2}} />
    </div>
    <div className="inline-block">
      <Toolkit__Ui_Coordinates coordinates={{latitude: 0., longitude: 0.}} />
    </div>
  </div>
}
