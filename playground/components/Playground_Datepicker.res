@react.component
let make = () => {
  let (value, setValue) = React.useState((): option<ReactDayPicker.range> => None)

  <div className="flex flex-row gap-6">
    <Toolkit__Ui_Button onClick={_ => setValue(_ => None)}>
      {"External reset"->React.string}
    </Toolkit__Ui_Button>
    <ReactDayPicker.RangeDayPickerInput
      value={{
        from: ?value->Option.flatMap(r => r.from),
        to: ?value->Option.flatMap(r => r.to),
      }}
      onChange={range =>
        setValue(_ =>
          switch range {
          | Some({from, to}) => Some({from, to})
          | _ => None
          }
        )}
      resetButton=true
      allowEmpty=false
      withPresetRanges=true
      modifiers={{
        disabled: [ReactDayPicker.Matcher.make(Interval({after: Js.Date.make()}))],
      }}
    />
  </div>
}
