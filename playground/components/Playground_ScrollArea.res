let tags = Array.makeBy(50, i => `v1.2.0-beta.${i->Int.toString}`)

@react.component
let make = () => {
  <Toolkit__Ui_ScrollArea className="bg-white shadow-xl w-64 h-64">
    <div className={"py-4 px-5 "}>
      <div className="Text"> {"Tags"->React.string} </div>
      {tags
      ->Array.map(tag => <div className="Tag" key={tag}> {tag->React.string} </div>)
      ->React.array}
    </div>
  </Toolkit__Ui_ScrollArea>
}
