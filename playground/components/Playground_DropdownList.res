@react.component
let make = () => {
  let items: array<Toolkit__Ui_DropdownList.item> = [
    {
      icon: <ReactIcons.MdKeyboardArrowRight size={22} />,
      label: "Test"->React.string,
      onClick: () => Js.log("test"),
      className: "",
    },
    {
      icon: <ReactIcons.MdKeyboardArrowRight size={22} />,
      label: "Test 2"->React.string,
      onClick: () => Js.log("test 2"),
      className: "",
    },
    {
      icon: <ReactIcons.MdKeyboardArrowRight size={22} />,
      label: "Test 3"->React.string,
      onClick: () => Js.log("test 3"),
      className: "",
    },
  ]

  <div className="flex flex-row gap-8 relative">
    <Toolkit__Ui_DropdownList label={"default bottom"->React.string} items />
    <Toolkit__Ui_DropdownList label={"top"->React.string} position=#top items />
    <div className="absolute top-20 left-40">
      <Toolkit__Ui_DropdownList label={"test"->React.string} items />
    </div>
    <div className="absolute top-20 right-0">
      <Toolkit__Ui_DropdownList label={"test 2"->React.string} defaultIsOpen={true} items />
    </div>
  </div>
}
