module DefaultExample = {
  @react.component
  let make = () => {
    let (value, setValue) = React.useState(() => None)

    <div>
      <h4> {"Simple"->React.string} </h4>
      <Toolkit__Ui_Autocomplete
        value
        onSelect={value => setValue(_ => value)}
        containerClassName={"w-64"}
        placeholder="Write 3 letter"
        minSearchLength=3
        additionnalOption={<p className="p-2 text-base">
          <ReactIntl.FormattedMessage
            id="createDelivery.addressNotFoundClickHere"
            defaultMessage="Adresse introuvable ? <a>Cliquez ici</a>"
            values={
              "a": children =>
                <a
                  className="text-info-500 cursor-pointer hover:underline"
                  onClick={_ => Js.log("")}>
                  children
                </a>,
            }
          />
        </p>}
        onSearch={_search => {
          let fakeResponse: Toolkit__Ui_Autocomplete.suggestions<string> = [
            {
              label: "Test 1",
              value: "1",
            },
            {
              label: "Test 2",
              value: "2",
            },
            {
              label: "Test 3",
              value: "3",
            },
          ]

          Toolkit.Utils.wait(0)->Promise.mapOk(_ => {
            fakeResponse
          })
        }}
      />
    </div>
  }
}

@react.component
let make = () =>
  <div>
    <DefaultExample />
    <div className="mt-4">
      <h4> {"Default value"->React.string} </h4>
      <Toolkit__Ui_Autocomplete
        onSelect={value => Js.log(value)}
        containerClassName={"w-64"}
        value={Some({
          label: "Test 1",
          value: "1",
        })}
        onSearch={_search => {
          let fakeResponse: Toolkit__Ui_Autocomplete.suggestions<string> = [
            {
              label: "Test 1",
              value: "1",
            },
            {
              label: "Test 2",
              value: "2",
            },
            {
              label: "Test 3",
              value: "3",
            },
          ]

          Toolkit.Utils.wait(3000)->Promise.mapOk(_ => {
            fakeResponse
          })
        }}
      />
    </div>
    <div className="mt-4">
      <h4> {"Empty search"->React.string} </h4>
      <Toolkit__Ui_Autocomplete
        onSelect={value => Js.log(value)}
        value={None}
        containerClassName={"w-64"}
        emptySearchClassName="text-sm"
        onSearch={_search => {
          let fakeResponse: Toolkit__Ui_Autocomplete.suggestions<string> = []

          Promise.resolved(Ok(fakeResponse))
        }}
      />
    </div>
    <div className="mt-4">
      <h4> {"Error search"->React.string} </h4>
      <Toolkit__Ui_Autocomplete
        onSelect={value => Js.log(value)}
        containerClassName={"w-64"}
        value={None}
        isInvalid=true
        emptySearchClassName="text-sm"
        onSearch={_search => {
          Promise.resolved(Error("ter"))
        }}
      />
    </div>
  </div>
