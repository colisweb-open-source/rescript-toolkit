@react.component
let make = () =>
  <div>
    <Toolkit__Ui_Dropdown dropdownClassName="w-60" label={"overflow"->React.string}>
      <div> {"Dropdown content !"->React.string} </div>
    </Toolkit__Ui_Dropdown>
    <div className="mt-10">
      <Toolkit__Ui_Dropdown
        dropdownClassName="w-60" position=#right label={"on the right"->React.string}>
        <div> {"Dropdown content !"->React.string} </div>
      </Toolkit__Ui_Dropdown>
    </div>
    <div className="mt-10">
      <Toolkit__Ui_Dropdown
        dropdownClassName="w-60" position=#top label={"on the top"->React.string}>
        <div> {"Dropdown content !"->React.string} </div>
      </Toolkit__Ui_Dropdown>
    </div>
    <div className="mt-10">
      <Toolkit__Ui_Dropdown
        dropdownClassName="w-60"
        position=#left
        label={"should be on the left but out of screen"->React.string}>
        <div> {"Dropdown content !"->React.string} </div>
      </Toolkit__Ui_Dropdown>
    </div>
    <div className="mt-8 w-60 mx-auto">
      <Toolkit__Ui_Dropdown
        buttonVariant=#outline
        buttonColor=#primary
        label={"Toggle me"->React.string}
        dropdownClassName="w-[300px]">
        <div> {"Dropdown content with long content !"->React.string} </div>
      </Toolkit__Ui_Dropdown>
      <div className="mt-10">
        <Toolkit__Ui_Dropdown
          buttonVariant=#outline
          position=#right
          buttonColor=#primary
          label={"Toggle me"->React.string}
          dropdownClassName="w-[300px]">
          <div> {"Dropdown content with long content !"->React.string} </div>
        </Toolkit__Ui_Dropdown>
      </div>
    </div>
  </div>
