@val
external alert: string => unit = "alert"

@react.component
let make = () => {
  let sizes = Js.Dict.fromArray([("md", #md), ("lg", #xl)])

  <div>
    {sizes
    ->Js.Dict.entries
    ->Array.mapWithIndex((i, (sizeText, size)) =>
      <div key={i->Int.toString ++ sizeText} className="flex flex-row mb-4 items-center">
        <strong className="w-40"> {sizeText->React.string} </strong>
        <Toolkit__Ui_ButtonGroup2
          onIndexChange={(currentIndex, tabValue) => {
            Js.log(currentIndex)
            Js.log(tabValue)
          }}
          buttons={[
            {label: "Option 1"->React.string, value: "test", count: 10},
            {label: "Option 2"->React.string, onClick: _ => alert("clicked on option 2")},
            {label: "Option 3"->React.string},
          ]}
          className="my-4 self-center"
          size
        />
      </div>
    )
    ->React.array}
  </div>
}
