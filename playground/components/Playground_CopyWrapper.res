@react.component
let make = () => {
  <ReactIntl.IntlProvider>
    <Toolkit__Ui_Snackbar.Provider />
    <div className="flex flex-col gap-8">
      <Toolkit__Ui_CopyWrapper textToCopy={"some content"} intl={PlaygroundLocales.intl}>
        {"some content"->React.string}
      </Toolkit__Ui_CopyWrapper>
    </div>
  </ReactIntl.IntlProvider>
}
