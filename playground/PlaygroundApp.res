let links: array<PlaygroundRouter.navLink> = [
  Single(
    App({
      route: Home,
      icon: <ReactIcons.MdHome size={30} />,
      label: "Home"->React.string,
    }),
  ),
  Grouped(
    {
      icon: <ReactIcons.FaBook size={30} />,
      label: "Docs"->React.string,
    },
    [
      App({
        label: "Form"->React.string,
        route: Docs(Form),
      }),
      App({
        label: "Identifiers"->React.string,
        route: Docs(Identifiers),
      }),
      App({
        label: "Requests"->React.string,
        route: Docs(Requests),
      }),
      App({
        label: "Api decoding"->React.string,
        route: Docs(ApiDecoding),
      }),
      App({
        label: "Primitives"->React.string,
        route: Docs(Primitives),
      }),
    ],
  ),
  Grouped(
    {
      icon: <ReactIcons.BsPalette size={30} />,
      label: "Design"->React.string,
    },
    [
      App({
        label: "Fonts"->React.string,
        route: DesignSystem(Fonts),
      }),
      App({
        label: "Colors"->React.string,
        route: DesignSystem(Colors),
      }),
      App({
        label: "MediaQueries"->React.string,
        route: DesignSystem(MediaQueries),
      }),
    ],
  ),
  Single(
    App({
      label: "Components"->React.string,
      icon: <ReactIcons.SiDatabricks size={30} />,
      route: Components(List),
    }),
  ),
  Single(
    App({
      label: "Hooks"->React.string,
      icon: <ReactIcons.GiHook size={30} />,
      route: Hooks(List),
    }),
  ),
  Single(
    App({
      label: "Bindings"->React.string,
      icon: <ReactIcons.VscFolderLibrary size={30} />,
      route: Bindings(List),
    }),
  ),
]

@val
external baseUrl: string = "import.meta.env.BASE_URL"

@react.component
let make = () => {
  <PlaygroundLocales.Provider>
    <PlaygroundRouter.Provider>
      {(~currentRoute: PlaygroundRouter.RouterConfig.t) => {
        <Toolkit.Ui.Layout.App
          onLogoClick={() => {
            PlaygroundRouter.redirect(Home)
          }}
          environment={"ReScript toolkit"}
          logoSrc={`${baseUrl}assets/logo.svg`}
          sideNavRender={(
            {
              onLinkClick,
              isNavOpen,
              openMenu,
            }: Toolkit.Ui.Layout.App.LeftNavigationBar.sideNavRender,
          ) => {
            <div>
              {links
              ->Array.mapWithIndex((i, link) =>
                <React.Fragment key={"nav-item" ++ i->Int.toString}>
                  <div className="my-3">
                    {switch link {
                    | Single(link) => <PlaygroundRouter.SingleLink link isNavOpen onLinkClick />
                    | Grouped(groupInfo, links) =>
                      <PlaygroundRouter.GroupedLinks
                        openMenu groupInfo links isNavOpen onLinkClick
                      />
                    }}
                  </div>
                </React.Fragment>
              )
              ->React.array}
            </div>
          }}>
          <PlaygroundRouter.Breadcrumb />
          <div className="p-4">
            {switch currentRoute {
            | Home => <Playground_Docs.Setup />
            | Docs(doc) =>
              switch doc {
              | Identifiers => <Playground_Docs.Identifiers />
              | ApiDecoding => <Playground_Docs.ApiDecoding />
              | Form => <Playground_Docs.Form />
              | Requests => <Playground_Docs.Requests />
              | Primitives => <Playground_Docs.Primitives />
              }
            | DesignSystem(route) =>
              switch route {
              | Fonts => <Playground_DesignSystem.Fonts />
              | Colors => <Playground_DesignSystem.Colors />
              | MediaQueries => <Playground_DesignSystem.MediaQueries />
              }

            | Components(component) =>
              switch component {
              | List => <PlaygroundComponents.List />
              | Details(component) => <PlaygroundComponents.Details component />
              }
            | Hooks(component) =>
              switch component {
              | List => <PlaygroundHooks.List />
              | Details(component) => <PlaygroundHooks.Details component />
              }
            | Bindings(component) =>
              switch component {
              | List => <PlaygroundBindings.List />
              | Details(component) => <PlaygroundBindings.Details component />
              }

            | NotFound => "NotFound"->React.string
            }}
          </div>
        </Toolkit.Ui.Layout.App>
      }}
    </PlaygroundRouter.Provider>
  </PlaygroundLocales.Provider>
}

let default = make
