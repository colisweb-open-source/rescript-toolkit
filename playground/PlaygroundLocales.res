@module("@root/locale/fr.json")
external toolkitLocalesFr: array<Toolkit.Intl.translation> = "default"

include Toolkit.Intl.Make({
  let messages: Toolkit.Intl.messages = {
    fr: Array.concatMany([toolkitLocalesFr]),
  }
  let defaultLocale = None
  let onError = msg => Js.log(msg)
})
