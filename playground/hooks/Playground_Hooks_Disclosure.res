let resi = ""

@module("@root/playground/hooks/Playground_Hooks_Disclosure.res?raw")
external codeExample: string = "default"

@react.component
let make = () => {
  let disclosure = Toolkit.Hooks.useDisclosure()

  <div className="grid grid-cols-2 gap-4 w-64">
    <div className="grid grid-rows-3 gap-4">
      <Toolkit.Ui.Button onClick={_ => disclosure.toggle()}>
        {"Toggle"->React.string}
      </Toolkit.Ui.Button>
      <Toolkit.Ui.Button onClick={_ => disclosure.show()}>
        {"Show"->React.string}
      </Toolkit.Ui.Button>
      <Toolkit.Ui.Button onClick={_ => disclosure.hide()}>
        {"Hide"->React.string}
      </Toolkit.Ui.Button>
    </div>
    <p> {(disclosure.isOpen ? "Visible" : "hidden")->React.string} </p>
  </div>
}
