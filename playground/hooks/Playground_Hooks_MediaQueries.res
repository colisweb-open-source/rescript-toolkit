let resi = ""

@module("@root/playground/hooks/Playground_Hooks_MediaQueries.res?raw")
external codeExample: string = "default"

@react.component
let make = () => {
  let result = Toolkit__Hooks.useMediaQuery()

  <div> {result->Obj.magic->Js.Json.stringifyWithSpace(2)->React.string} </div>
}
