const sass = require("sass");
const path = require("path");
const fs = require("fs");
const appDirectory = fs.realpathSync(process.cwd());

const FILENAMES = ["Toolkit__Ui_DatetimeInput", "styles"];

for (const FILENAME of FILENAMES) {
  const file = path.resolve(appDirectory, `src/ui/${FILENAME}.scss`);
  const outFile = path.resolve(appDirectory, `src/ui/${FILENAME}.css`);

  const result = sass.renderSync({
    file,
    includePaths: [path.resolve(appDirectory, "node_modules")],
    outFile,
  });

  fs.writeFile(outFile, result.css, function (err) {
    if (!err) {
      console.log("✅ done");
    }
  });
}
