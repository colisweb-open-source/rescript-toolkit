import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import createReScriptPlugin from "@jihchi/vite-plugin-rescript";
import mdx from "@mdx-js/rollup";
import fs from "fs";
import addClasses from "rehype-add-classes";
import rehypeHighlight from "rehype-highlight";
import rescriptHighlight from "./playground/utils/rescript-highlight";
import rehypeAutolinkHeadings from "rehype-autolink-headings";
import rehypeSlug from "rehype-slug";
import { viteCommonjs } from "@originjs/vite-plugin-commonjs";

const appDirectory = fs.realpathSync(process.cwd());
const isProduction = process.env.NODE_ENV === "production";

export default defineConfig({
  base: isProduction ? "/colisweb-open-source/rescript-toolkit/" : "/",
  build: {
    sourcemap: true,
  },
  plugins: [
    viteCommonjs(),
    mdx({
      rehypePlugins: [
        [
          addClasses,
          {
            h1: "text-4xl",
            h2: "text-3xl",
            h3: "text-2xl",
            h4: "text-xl",
            "h1,h2,h3,h4": "font-display font-bold underline mb-1",
            p: "mb-1",
            pre: "bg-white p-2 rounded-lg border my-4 block",
            ul: "list-disc",
          },
        ],
        [rehypeSlug],
        [rehypeAutolinkHeadings, { behavior: "wrap" }],
        [
          rehypeHighlight,
          {
            languages: {
              rescript: rescriptHighlight,
            },
          },
        ],
      ],
    }),
    createReScriptPlugin(),
    react(),
  ],
  define: {
    global: {},
  },
  optimizeDeps: {
    include: ["highlight.js"],
  },
  resolve: {
    alias: [
      {
        find: "@root",
        replacement: (_) => appDirectory,
      },
    ],
  },
});
