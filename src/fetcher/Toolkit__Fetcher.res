module type Config = {
  module Request: Toolkit__Request.Config
  let key: Request.argument => array<string>
}

module Make = (Config: Config) => {
  let key: Config.Request.argument => Swr.SwrKey.t = arr =>
    Config.key(arr)->Js.Array2.joinWith("--")->Swr.SwrKey.make

  let use = (
    ~options: option<Swr.fetcherOptions>=?,
    argument: option<Config.Request.argument>,
  ): Toolkit__Hooks.fetcher<Config.Request.response> =>
    Toolkit__Hooks.useFetcher(~options?, argument->Option.map(key), () => {
      Config.Request.exec(argument->Option.getExn)->Promise.Js.fromResult
    })

  let useOptional = (
    ~options: option<Swr.fetcherOptions>=?,
    argument: option<Config.Request.argument>,
  ): Toolkit__Hooks.fetcher<option<Config.Request.response>> =>
    Toolkit__Hooks.useOptionalFetcher(~options?, argument->Option.map(key), () => {
      Config.Request.exec(argument->Option.getExn)->Promise.Js.fromResult
    })
}

module type MutationConfig = {
  module Request: Toolkit__Request.Config
  let key: string
}

module MakeMutation = (Config: MutationConfig) => {
  let key = Config.key->Swr.SwrKey.make

  let useMutation = (
    ~options: option<Swr.mutationFetcherOptions<Config.Request.response>>=?,
    key: string,
  ) => {
    Swr.useSwrMutation(
      Swr.SwrKey.make(key),
      (_url, {arg}) => {
        Config.Request.exec(arg)->Promise.Js.fromResult
      },
      switch options {
      | Some(options) => options->Obj.magic
      | None => {revalidate: true}
      },
    )
  }
}
