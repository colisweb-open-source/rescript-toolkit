open ReactIntl

module DatePicker = {
  module Fr = {
    let weekdaysShort = ["Di", "Lun", "Ma", "Me", "Je", "Ve", "Sa"]

    let months = [
      `Janvier`,
      `Février`,
      `Mars`,
      `Avril`,
      `Mai`,
      `Juin`,
      `Juillet`,
      `Août`,
      `Septembre`,
      `Octobre`,
      `Novembre`,
      `Décembre`,
    ]

    let firstDayOfWeek = 1
  }
}

module Days = {
  @@intl.messages
  let monday = {defaultMessage: "Lundi"}
  let tuesday = {defaultMessage: "Mardi"}
  let wednesday = {defaultMessage: "Mercredi"}
  let thursday = {defaultMessage: "Jeudi"}
  let friday = {defaultMessage: "Vendredi"}
  let saturday = {defaultMessage: "Samedi"}
  let sunday = {defaultMessage: "Dimanche"}
}
