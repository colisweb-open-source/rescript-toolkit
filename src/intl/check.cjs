const fs = require("fs");
const cp = require("child_process");
const path = require("path");
const cwd = process.cwd();
const translations = path.join(cwd, "locale");

const AVAILABLE_LANGUAGES = ["fr"];

const missingMessagesLanguages = AVAILABLE_LANGUAGES.map((locale) => {
  const file = path.join(translations, `${locale}.json`);
  const content = JSON.parse(fs.readFileSync(file));

  return [content.filter((item) => item.message === "").length, locale];
});

missingMessagesLanguages.forEach(([missingMessagesLanguage, language]) => {
  if (missingMessagesLanguage > 0) {
    console.log(
      `=== ⚠️ [${language}]   There are ${missingMessagesLanguage} messages missing.`
    );
  }
});

if (missingMessagesLanguages.some(([missingMessages]) => missingMessages > 0)) {
  process.exit(1);
}
