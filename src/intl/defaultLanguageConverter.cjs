const fs = require("fs");
const path = require("path");
const glob = require("glob");

const cwd = process.cwd();
const originFile = path.join(cwd, "locale/translations/fr.json");
const base = JSON.parse(fs.readFileSync(originFile, "utf8"));
const dict = base.reduce((acc, value) => {
  acc[value.defaultMessage] = value.message;
  return acc;
}, {});
/**
 * Match :
 * - defaultMessage=""
 * - defaultMessage={""}
 * - defaultMessage:
 */
const pattern = /(defaultMessage(=|:|={|:\s))"(.*?|$)"/g;

glob("**/*.res", (err, files) => {
  files.forEach((file) => {
    const filePath = path.join(cwd, file);
    let fileContent = fs.readFileSync(filePath, "utf-8");
    const matches = fileContent.matchAll(pattern);

    for (const match of matches) {
      let [, , , key] = match;
      let message = dict[key];

      if (message) {
        fileContent = fileContent.replace(`"${key}"`, `"${message}"`);
      }
    }

    fs.writeFileSync(filePath, fileContent);
  });
});
