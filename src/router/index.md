# Router

Type-safe navigation

## Usage

```rescript
open Toolkit;

module StoreId = Identifiers.MakeString({});

module RouterConfig = {
  type admin =
    | Home;

  type t =
    | Home
    | StoreDetails(StoreId.t)
    | Admin(admin)
    | NotFound;

  let make = (url) => switch (url.path) {
    | [] => Home
    | ["stores", storeId] => StoreDetails(StoreId.make(storeId))
    | ["admin", ...rest] => {
      switch (rest) {
        | [] => Admin(Home)
        | _ => NotFound
      }
    | _ => NotFound
    }
  };

  let toString = (route) => switch (route) {
    | Home => ""
    | StoreDetails(storeId) => "/stores/"++ storeId->StoreId.toString
    | Admin(Home) => "/admin/"
    | NotFound => "/404"
  };
};

module Navigation = Router.Make(RouterConfig);
```
