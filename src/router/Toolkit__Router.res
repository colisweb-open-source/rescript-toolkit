module type RouterConfig = {
  type t
  let make: RescriptReactRouter.url => t
  let toString: t => string
}

module Make = (Config: RouterConfig) => {
  type contextState = {
    currentRoute: Config.t,
    previousRoute: option<Config.t>,
  }
  let navigate = (route: Config.t) => RescriptReactRouter.push(route->Config.toString)

  let replace = (route: Config.t) => RescriptReactRouter.replace(route->Config.toString)

  let routerContext = React.createContext({
    currentRoute: RescriptReactRouter.dangerouslyGetInitialUrl()->Config.make,
    previousRoute: None,
  })

  module RouterContextProvider = {
    let make = React.Context.provider(routerContext)
  }

  module Provider = {
    @react.component
    let make = (~children) => {
      let (currentRoute, setCurrentRoute) = React.useState(() =>
        RescriptReactRouter.dangerouslyGetInitialUrl()->Config.make
      )

      let previousRoute = Toolkit__Hooks.usePrevious(currentRoute)

      React.useLayoutEffect(() => {
        let watcherID = RescriptReactRouter.watchUrl(url => setCurrentRoute(_ => url->Config.make))
        Some(() => RescriptReactRouter.unwatchUrl(watcherID))
      }, [setCurrentRoute])

      let contextValue: contextState = {
        previousRoute,
        currentRoute,
      }

      <RouterContextProvider value={contextValue}>
        {children(~currentRoute)}
      </RouterContextProvider>
    }
  }

  let useRoute = (): Config.t => RescriptReactRouter.useUrl()->Config.make

  let isRouteEqual = (routeA: Config.t, routeB: Config.t): bool => {
    let routeA = {
      let url = routeA->Config.toString
      let queryParamsIndex = url->Js.String2.indexOf("?")

      queryParamsIndex === -1 ? url : url->Js.String2.slice(~from=0, ~to_=queryParamsIndex)
    }

    let routeB = {
      let url = routeB->Config.toString
      let queryParamsIndex = url->Js.String2.indexOf("?")

      queryParamsIndex === -1 ? url : url->Js.String2.slice(~from=0, ~to_=queryParamsIndex)
    }

    routeA === routeB
  }

  let useIsCurrentRoute = (route: Config.t): bool => {
    let {currentRoute} = React.useContext(routerContext)
    isRouteEqual(currentRoute, route)
  }

  let navigate = (route: Config.t): unit => route->Config.toString->RescriptReactRouter.push

  let redirect = (route: Config.t): unit => route->Config.toString->RescriptReactRouter.replace

  let toString = Config.toString

  let usePreviousLinkWithDefault = (defaultRoute, cb) => {
    let {previousRoute} = React.useContext(routerContext)
    previousRoute->Option.mapWithDefault(defaultRoute, route => {
      cb(route) ? route : defaultRoute
    })
  }

  module Link = {
    @react.component
    let make = (
      ~route: Config.t,
      ~className="",
      ~activeClassName="",
      ~onClick: option<ReactEvent.Mouse.t => unit>=?,
      ~isExternal=false,
      ~children,
    ) => {
      let isCurrentRoute = useIsCurrentRoute(route)
      let activeClassName = isCurrentRoute ? activeClassName : ""

      <a
        href={route->Config.toString}
        target={isExternal ? "_blank" : ""}
        className={cx([className, activeClassName])}
        onClick={event =>
          if (
            !(event->ReactEvent.Mouse.defaultPrevented) &&
            (event->ReactEvent.Mouse.button == 0 &&
            (!(event->ReactEvent.Mouse.altKey) &&
            (!(event->ReactEvent.Mouse.ctrlKey) &&
            (!(event->ReactEvent.Mouse.metaKey) &&
            (!(event->ReactEvent.Mouse.shiftKey) &&
            !isExternal)))))
          ) {
            event->ReactEvent.Mouse.preventDefault
            onClick->Option.map(fn => fn(event))->ignore
            route->navigate
          }}>
        children
      </a>
    }
  }

  module Redirect = {
    type behavior =
      | Push
      | Replace

    @react.component
    let make = (~to_: Config.t, ~behavior=Replace) => {
      React.useEffect(() => {
        switch behavior {
        | Push => navigate(to_)
        | Replace => replace(to_)
        }
        None
      }, [])

      React.null
    }
  }

  module RedirectExternal = {
    @react.component
    let make = (~url: string) => {
      React.useEffect(() => {
        Browser.Location.replace(Browser.Location.location, url)

        None
      }, [])

      React.null
    }
  }

  module Breadcrumb = {
    type link = {
      route: Config.t,
      text: React.element,
    }
    type state = {links: option<array<link>>}

    type action =
      | Reset
      | Update(array<link>)

    let store = Restorative.createStore({links: None}, (_state, action) =>
      switch action {
      | Reset => {links: None}
      | Update(links) => {links: Some(links)}
      }
    )

    let use = (routes: array<link>) => {
      React.useLayoutEffect(() => {
        store.dispatch(Update(routes))

        Some(() => store.dispatch(Reset))
      }, [])
    }

    module Use = {
      @react.component
      let make = (~routes: array<link>) => {
        use(routes)
        React.null
      }
    }

    @react.component
    let make = () => {
      let {links} = store.useStore()

      links->Option.mapWithDefault(React.null, links => {
        <div className="flex flex-row items-center gap-4 p-4 border-b bg-white">
          {links
          ->Array.mapWithIndex((i, {route, text}) => {
            <React.Fragment key={i->Int.toString}>
              {i > 0 ? <ReactIcons.MdKeyboardArrowRight size=20 /> : React.null}
              {links->Toolkit__Primitives.Array.isLastIndex(i)
                ? <div className="font-semibold"> text </div>
                : <Link route className="hover:underline"> text </Link>}
            </React.Fragment>
          })
          ->React.array}
        </div>
      })
    }
  }

  type rec navLink =
    | Single(link)
    | Grouped(groupInfo, array<link>)
  and link =
    | Legacy(legacyLink)
    | App(appLink)
    | Custom(React.element)
  and groupInfo = {
    label: React.element,
    icon: React.element,
    hideContentWhenClosed?: bool,
  }
  and appLink = {
    route: Config.t,
    icon?: React.element,
    label: React.element,
    disabledActiveLink?: bool,
  }
  and legacyLink = {
    url: string,
    icon?: React.element,
    label: React.element,
  }

  module Divider = {
    @react.component
    let make = () => <div className="border-b" />
  }

  let commonClassName = "flex items-center py-3 hover:bg-primary-100 focus:bg-primary-100 focus:shadow-none text-neutral-700 rounded relative font-semibold transition-all ease-linear duration-200"

  module SingleLink = {
    @react.component
    let make = (
      ~link: link,
      ~isNavOpen: bool,
      ~isSubLink=false,
      ~isActive=false,
      ~onLinkClick=() => (),
    ) =>
      switch link {
      | Custom(element) => element
      | Legacy(config) =>
        let {label, url} = config
        <a
          href=url
          target="_blank"
          className={cx([commonClassName, "py-3 sidenav-link", isSubLink ? "ml-3" : ""])}>
          <span className="overflow-hidden flex">
            {config.icon->Option.mapWithDefault(React.null, icon =>
              <div className={cx(["text-neutral-800", isNavOpen ? "pl-2" : "px-2"])}> icon </div>
            )}
            <span
              className={cx([
                "transition-all duration-200 ease-in-out absolute ml-12 w-40 transform top-1/2 -translate-y-1/2",
                isNavOpen ? "pl-1 opacity-100 delay-75" : "opacity-0 invisible",
                isSubLink ? "text-sm" : "",
              ])}>
              label
            </span>
            {isNavOpen
              ? React.null
              : <div
                  className="sidenav-link-tooltip absolute left-0 p-2 bg-neutral-700 text-white rounded transform top-1/2 -translate-y-1/2 transition-all duration-200 ease-in-out opacity-0 invisible ml-16 whitespace-nowrap">
                  label
                </div>}
          </span>
        </a>
      | App(config) =>
        let {label, route} = config

        let disabledActiveLink = config.disabledActiveLink->Option.getWithDefault(false)

        <Link
          route
          className={cx([
            commonClassName,
            isSubLink ? "ml-3" : "",
            config.icon->Option.isNone ? "py-5" : "",
            "sidenav-link",
            isActive && !disabledActiveLink ? "bg-primary-100/75 text-neutral-700" : "",
          ])}
          activeClassName={disabledActiveLink ? "" : "bg-primary-100/75 text-neutral-700"}
          onClick={_ => onLinkClick()}>
          <span className="overflow-hidden flex">
            {config.icon->Option.mapWithDefault(React.null, icon =>
              <div className={cx(["text-neutral-800", isNavOpen ? "pl-2" : "px-2"])}> icon </div>
            )}
            <span
              className={cx([
                "transition-all duration-200 ease-in-out absolute ml-12 w-40 transform top-1/2 -translate-y-1/2",
                isNavOpen ? "pl-1 opacity-100 delay-75" : "opacity-0 invisible",
                isSubLink ? "text-sm" : "",
              ])}>
              label
            </span>
            {isNavOpen
              ? React.null
              : <div
                  className="sidenav-link-tooltip absolute left-0 p-2 bg-neutral-700 text-white rounded transform top-1/2 -translate-y-1/2 transition-all duration-200 ease-in-out opacity-0 invisible ml-16 whitespace-nowrap">
                  label
                </div>}
          </span>
        </Link>
      }
  }

  module GroupedLinks = {
    module LinksList = {
      @react.component
      let make = (~links, ~relativeRoute, ~isNavOpen, ~onLinkClick) => {
        links
        ->Array.mapWithIndex((i, link) => {
          let isActive = switch (relativeRoute, link) {
          | (Some(App({route: relativeRoute})), App({route})) => relativeRoute->isRouteEqual(route)
          | _ => false
          }
          <SingleLink
            key={"sidenav-grouped-" ++ i->Int.toString}
            link
            isNavOpen
            isSubLink=true
            onLinkClick
            isActive
          />
        })
        ->React.array
      }
    }

    @react.component
    let make = (
      ~groupInfo: groupInfo,
      ~links: array<link>,
      ~isNavOpen: bool,
      ~onLinkClick=() => (),
      ~openMenu,
    ) => {
      let (isOpen, setIsOpen) = React.useState(() => false)
      let currentRoute = useRoute()
      let toggle = React.useCallback(_ => setIsOpen(v => !v), [])
      let hasActiveSubRoute = links->Array.some(link => {
        switch link {
        | Legacy(_) => false
        | Custom(_) => false
        | App({route}) => isRouteEqual(currentRoute, route)
        }
      })
      let breadcrumb = Breadcrumb.store.useStore()

      let relativeRoute = breadcrumb.links->Option.mapWithDefault(None, breadcrumbLinks => {
        links->Array.getBy(link => {
          switch link {
          | Legacy(_) => false
          | Custom(_) => false
          | App({route}) =>
            breadcrumbLinks->Array.some(breadcrumbLink => breadcrumbLink.route->isRouteEqual(route))
          }
        })
      })

      React.useEffect(() => {
        if !isNavOpen {
          setIsOpen(_ => false)
        }
        None
      }, [isNavOpen])

      <React.Fragment>
        <button
          onClick={_ => {
            openMenu()
            toggle()
          }}
          className={cx([
            commonClassName,
            "flex items-center w-full sidenav-link",
            hasActiveSubRoute || relativeRoute->Option.isSome ? "bg-primary-100/75" : "",
          ])}>
          <span className={cx(["mr-2 text-neutral-800", isNavOpen ? "pl-2" : "px-2"])}>
            groupInfo.icon
          </span>
          <span
            className={cx([
              "transition-all duration-200 ease-in-out absolute ml-12 w-40 transform top-1/2 -translate-y-1/2 text-left",
              isNavOpen ? "pl-1 opacity-100 delay-75" : "opacity-0 invisible",
            ])}>
            groupInfo.label
          </span>
          <span className={cx(["absolute right-0 mr-2", isNavOpen ? "opacity-100" : "opacity-0"])}>
            <ReactIcons.MdKeyboardArrowRight
              size=24
              className={cx([
                "transition-all ease-linear duration-200 transform",
                isOpen ? "rotate-90" : "",
              ])}
            />
          </span>
          {isNavOpen
            ? React.null
            : <div
                className="sidenav-link-tooltip absolute left-0 p-2 bg-neutral-700 text-white rounded transform top-1/2 -translate-y-1/2 transition-all duration-200 ease-in-out opacity-0 invisible ml-16 whitespace-nowrap">
                groupInfo.label
              </div>}
        </button>
        <div
          className={cx([
            "transition ease-linear duration-300",
            isOpen ? "h-auto" : "h-0 overflow-hidden",
          ])}>
          {switch groupInfo.hideContentWhenClosed {
          | Some(true) =>
            isOpen ? <LinksList links relativeRoute onLinkClick isNavOpen /> : React.null
          | _ => <LinksList links relativeRoute onLinkClick isNavOpen />
          }}
        </div>
      </React.Fragment>
    }
  }
}
