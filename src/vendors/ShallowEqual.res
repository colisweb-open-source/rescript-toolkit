@module("shallow-equal")
external shallowEqualObjects: ('a, 'b) => bool = "shallowEqualObjects"

@module("shallow-equal")
external shallowEqualArrays: (array<'a>, array<'b>) => bool = "shallowEqualArrays"
