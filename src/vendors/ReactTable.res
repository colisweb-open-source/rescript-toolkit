type options<'data>
type column<'data>
type props
type plugin

type filterOption = {
  id: string,
  value: string,
}

type sorting =
  | @as("asc") Asc
  | @as("desc") Desc

type sortOption = {
  id: string,
  value: sorting,
  desc: bool,
}

type rec cellProps<'cellValue> = {cell: cell<'cellValue>}
and cell<'cellValue> = {value: 'cellValue}

type rec instance<'data> = {
  getTableProps: unit => props,
  getTableBodyProps: unit => props,
  columns: array<instanceColumn>,
  allColumns: array<instanceColumn>,
  headers: array<instanceColumn>,
  rows: array<instanceRow<'data>>,
  preFilteredRows: array<instanceRow<'data>>,
  page: array<instanceRow<'data>>,
  prepareRow: instanceRow<'data> => unit,
  canPreviousPage: bool,
  canNextPage: bool,
  pageOptions: array<int>, // ?
  pageCount: int,
  gotoPage: int => unit,
  nextPage: unit => unit,
  previousPage: unit => unit,
  setPageSize: int => unit,
  state: state,
  setAllFilters: array<filterOption> => unit,
  setFilter: (string, string) => unit,
  toggleSortBy: (~column: string, ~descending: bool, ~isMulti: bool) => unit,
  setSortBy: array<sortOption> => unit,
}
and state = {
  pageIndex: int,
  pageSize: int,
  filters: array<currentFilter>,
  sortBy: array<sortBy>,
}
and sortBy = {
  id: string,
  value?: sorting,
  desc?: bool,
}
and currentFilter = {
  value: string,
  id: string,
}
and instanceColumn = {
  id: string,
  isVisible: bool,
  toggleHidden: unit => unit,
  totalLeft: int,
  totalWidth: int,
  render: (string, unit) => React.element,
  getHeaderProps: unit => props,
  getFooterProps: unit => props,
  getResizerProps: unit => props,
  getSortByToggleProps: unit => props,
  clearSortBy: unit => unit,
  isResizing: bool,
  isSortedDesc: option<bool>,
  isSorted: bool,
}
and instanceRow<'data> = {
  cells: array<instanceCell>,
  index: int,
  original: 'data,
  getRowProps: unit => props,
}
and instanceCell = {
  getCellProps: unit => props,
  render: (string, unit) => React.element,
}

type rec filterProps<'filterValue> = {column: filterColumn<'filterValue>, state: state}
and filterColumn<'filterValue> = {
  filterValue: option<'filterValue>,
  setFilter: option<'filterValue> => unit,
  id: string,
}

@obj
external makeColumn: (
  ~accessor: 'data => 'cellValue,
  ~id: string,
  ~header: React.element,
  ~filterRender: filterProps<'filterValue> => React.element=?,
  ~filter: string=?,
  ~cell: cellProps<'cellValue> => React.element=?,
  ~minWidth: int=?,
  ~width: int=?,
  ~disableSortBy: bool=?,
  unit,
) => column<'data> = ""

@obj
external make: (
  ~columns: array<column<'data>>,
  ~data: array<'data>,
  ~debug: bool=?,
  ~initialState: 'initialState=?,
  ~useControlledState: 'initialState => 'initialState=?,
  ~manualPagination: bool=?,
  ~manualFilters: bool=?,
  ~manualSortBy: bool=?,
  ~pageCount: option<int>=?,
  ~disableFilters: bool=?,
  ~disableMultiSort: bool=?,
  unit,
) => options<array<'data>> = ""

@module("react-table") external usePagination: plugin = "usePagination"
@module("react-table") external useSortBy: plugin = "useSortBy"
@module("react-table") external useFilters: plugin = "useFilters"
@module("react-table")
external useAbsoluteLayout: plugin = "useAbsoluteLayout"
@module("react-table")
external useResizeColumns: plugin = "useResizeColumns"
@module("react-table") external useBlockLayout: plugin = "useBlockLayout"
@module("react-table") external useFlexLayout: plugin = "useFlexLayout"
@module("react-table") external useRowSelect: plugin = "useRowSelect"

@module("react-table")
external useTable0: options<array<'data>> => instance<'data> = "useTable"

@module("react-table")
external useTable1: (options<'data>, plugin) => instance<'data> = "useTable"

@module("react-table")
external useTable2: (options<'data>, plugin, plugin) => instance<'data> = "useTable"

@module("react-table")
external useTable3: (options<'data>, plugin, plugin, plugin) => instance<'data> = "useTable"

@module("react-table")
external useTable4: (options<'data>, plugin, plugin, plugin, plugin) => instance<'data> = "useTable"
@module("react-table")
external useTable5: (options<'data>, plugin, plugin, plugin, plugin, plugin) => instance<'data> =
  "useTable"
@module("react-table")
external useTable6: (
  options<'data>,
  plugin,
  plugin,
  plugin,
  plugin,
  plugin,
  plugin,
) => instance<'data> = "useTable"

@module("react-table")
external useTable7: (
  options<'data>,
  plugin,
  plugin,
  plugin,
  plugin,
  plugin,
  plugin,
  plugin,
) => instance<'data> = "useTable"

//    Utils :

let getFiltersDictFromArray = (filters: array<currentFilter>): Js.Dict.t<string> =>
  Js.Dict.fromArray(filters->Array.map(filter => (filter.id, filter.value)))
