type revalidateOptions

@obj
external makeRevalidateOptions: (
  ~retryCount: option<int>=?,
  ~dedupe: option<bool>=?,
  unit,
) => revalidateOptions = ""

type fetcherOptions

@obj
external makeFetcherOptions: (
  ~suspense: bool=?,
  ~keepPreviousData: bool=?,
  ~fetcher: 'fetcher=?,
  ~fallbackData: 'data=?,
  ~revalidateOnFocus: bool=?,
  ~revalidateOnMount: bool=?,
  ~revalidateOnReconnect: bool=?,
  ~refreshInterval: int=?,
  ~refreshWhenHidden: bool=?,
  ~refreshWhenOffline: bool=?,
  ~shouldRetryOnError: bool=?,
  ~dedupingInterval: int=?,
  ~focusThrottleInterval: int=?,
  ~loadingTimeout: int=?,
  ~errorRetryInterval: int=?,
  ~onLoadingSlow: (string, fetcherOptions) => unit=?,
  ~onSuccess: ('data, string, fetcherOptions) => unit=?,
  ~onError: (Js.Exn.t, string, fetcherOptions) => unit=?,
  ~onErrorRetry: (
    Js.Exn.t,
    string,
    fetcherOptions,
    revalidateOptions => Js.Promise.t<bool>,
    revalidateOptions,
  ) => unit=?,
  ~compare: (Js.Nullable.t<'data>, Js.Nullable.t<'data>) => bool=?,
  unit,
) => fetcherOptions = ""

type fetcher<'data> = {
  data: option<'data>,
  error: option<Js.Exn.t>,
  isValidating: bool,
  isLoading: bool,
  mutate: (. unit) => Js.Promise.t<bool>,
}

type mutationFetcherOptions<'optimisticData> = {
  optimisticData?: 'optimisticData => 'optimisticData,
  revalidate?: bool,
  populateCache?: bool,
}

type mutationFetcher<'params, 'data, 'error> = {
  data: option<'data>,
  error: option<Js.Exn.t>,
  isMutating: bool,
  trigger: 'params => Promise.Js.t<'data, 'error>,
}

module SwrKey = Toolkit__Identifier.MakeString()

@unboxed
type key =
  | String(string)
  | Array(array<string>)

@module("swr")
external useSwr: (option<SwrKey.t>, 'fn, 'fetcherOptions) => fetcher<'data> = "default"

type mutationParams<'params> = {arg: 'params}
@module("swr/mutation")
external useSwrMutation: (
  SwrKey.t,
  (string, mutationParams<'params>) => 'response,
  mutationFetcherOptions<'optimisticData>,
) => mutationFetcher<'params, 'data, 'error> = "default"

@module("swr")
external useSwrOptional: (option<SwrKey.t>, 'fn, 'fetcherOptions) => fetcher<option<'data>> =
  "default"

type cache
@module("swr") external cache: cache = "cache"
@send external clear: cache => unit = "clear"
@send external delete: (cache, option<SwrKey.t>) => unit = "delete"

module SwrConfig = {
  type t

  @send
  external mutate0: (t, SwrKey.t) => unit = "mutate"
  @send
  external mutateByKey: (t, key) => unit = "mutate"
  @send
  external mutateByKeyWithParams: (t, key, 'data, bool) => unit = "mutate"

  @send
  external mutate: (t, SwrKey.t, 'data, bool) => unit = "mutate"

  @module("swr") @react.component
  external make: (~value: fetcherOptions=?, ~children: React.element) => React.element = "SWRConfig"
}

@module("swr")
external useSWRConfig: unit => SwrConfig.t = "useSWRConfig"
