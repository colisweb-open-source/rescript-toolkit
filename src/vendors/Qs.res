@module("qs") external stringify: Js.t<'a> => string = "stringify"

type arrayFormat =
  | @as("indices") Indices
  | @as("comma") Comma
  | @as("brackets") Brackets
  | @as("repeat") Repeat

type params = {
  addQueryPrefix?: bool,
  serializeDate?: Js.Date.t => string,
  arrayFormat?: arrayFormat,
}
@obj
external makeParams: (
  ~addQueryPrefix: bool=?,
  ~serializeDate: Js.Date.t => string=?,
  unit,
) => params = ""

@module("qs")
external stringifyWithParams: (Js.Json.t, params) => string = "stringify"

@module("qs") external stringifyJson: Js.Json.t => string = "stringify"

@module("qs") external parse: string => Js.Dict.t<string> = "parse"
