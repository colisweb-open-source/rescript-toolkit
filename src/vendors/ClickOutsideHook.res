@module("click-outside-hook")
external useClickOutside: (unit => unit) => React.ref<Js.Nullable.t<'a>> = "default"
