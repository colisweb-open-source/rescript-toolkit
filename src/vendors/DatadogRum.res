module Browser = {
  type initOptions = {
    applicationId: string,
    clientToken: string,
    site?: string,
    service: string,
    env: string,
    sessionSampleRate?: int,
    sessionReplaySampleRate?: int,
    defaultPrivacyLevel?: string,
    allowedTracingOrigins?: array<Js.Re.t>,
    allowedTracingUrls?: array<Js.Re.t>,
    silentMultipleInit?: bool,
    enableExperimentalFeatures?: array<string>,
    trackResources?: bool,
    trackLongTasks?: bool,
    trackUserInteractions?: bool,
  }
  @module("@datadog/browser-rum") @scope("datadogRum")
  external datadogRumInit: initOptions => unit = "init"

  @module("@datadog/browser-rum") @scope("datadogRum")
  external startSessionReplayRecording: 'a => unit = "startSessionReplayRecording"

  @module("@datadog/browser-rum") @scope("datadogRum")
  external addAction: (string, 'a) => unit = "addAction"

  @module("@datadog/browser-rum") @scope("datadogRum")
  external setUserProperty: (string, 'a) => unit = "setUserProperty"

  @module("@datadog/browser-rum") @scope("datadogRum")
  external setUser: 'a => unit = "setUser"

  @module("@datadog/browser-rum") @scope("datadogRum")
  external addAction: (string, 'a) => unit = "addAction"

  @module("@datadog/browser-rum") @scope("datadogRum")
  external addError: Js.Exn.t => unit = "addError"

  @module("@datadog/browser-rum") @scope("datadogRum")
  external addErrorWithContext: (Js.Exn.t, 'context) => unit = "addError"

  let use = (~options: initOptions, ~sessionData, ~contextCallback=?, ()) => {
    React.useLayoutEffect(() => {
      if options.env !== "local" {
        datadogRumInit({
          ...options,
          site: "datadoghq.com",
          allowedTracingUrls: [%re("/https:\/\/api.*\.colisweb\.com/")],
          allowedTracingOrigins: [
            %re("/https:\/\/api.*\.colisweb\.com/"),
            %re("/https:\/\/bo.*\.colisweb\.com/"),
          ],
          defaultPrivacyLevel: "allow",
          enableExperimentalFeatures: ["clickmap"],
          trackResources: true,
          trackLongTasks: true,
          trackUserInteractions: true,
        })

        startSessionReplayRecording()
      }
      None
    }, [])

    React.useLayoutEffect(() => {
      if options.env !== "local" {
        setUser(sessionData)

        setUserProperty(
          "screen_resolution",
          `${Browser.screenWidth->Int.toString}x${Browser.screenHeight->Int.toString}`,
        )

        contextCallback->Option.forEach(fn => fn(setUserProperty))
      }

      None
    }, [sessionData->Js.Json.stringifyAny])
  }
}
