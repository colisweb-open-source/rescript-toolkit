@@warning("-22")
@@warning("-27")

type range = {
  from?: Js.Date.t,
  to?: Js.Date.t,
}
module DateUtils = {
  @module("react-day-picker")
  external addToRange: (Js.Date.t, range) => range = "addToRange"
}

type classnames = {
  container: string,
  overlayWrapper: string,
  overlay: string,
}

type mode = [#single | #multiple | #range | #default]

type formatters = {
  formatCaption?: Js.Date.t => React.element,
  formatDay?: Js.Date.t => React.element,
  formatMonthCaption?: Js.Date.t => React.element,
  formatWeekNumber?: int => React.element,
  formatWeekdayName?: Js.Date.t => React.element,
  formatYearCaption?: Js.Date.t => React.element,
}

type modifiersClassNames = {
  disabled?: string,
  hidden?: string,
  outside?: string,
  range_end?: string,
  range_middle?: string,
  range_start?: string,
  selected?: string,
  today?: string,
  closed?: string,
  clicked?: string,
}

type interval = {before?: Js.Date.t, after?: Js.Date.t}

module Matcher = {
  @unboxed
  type rec t = Any('a): t
  and case = Date(Js.Date.t) | DayOfWeek(int) | Range(range) | Interval(interval) | Bool(bool)

  let date = (v: Js.Date.t) => Any(v)
  let dayOfWeek = (v: int) => Any({"dayOfWeek": v})
  let range = (v: range) => Any(v)
  let interval = (v: interval) => Any(v)
  let bool = (v: bool) => Any(v)

  let make = (case: case): t =>
    switch case {
    | Date(v) => v->date
    | DayOfWeek(v) => v->dayOfWeek
    | Range(v) => v->range
    | Interval(v) => v->interval
    | Bool(v) => v->bool
    }
}

type modifiers = {
  disabled?: array<Matcher.t>,
  hidden?: array<Matcher.t>,
  outside?: array<Matcher.t>,
  range_end?: array<Matcher.t>,
  range_middle?: array<Matcher.t>,
  range_start?: array<Matcher.t>,
  selected?: array<Matcher.t>,
  today?: array<Matcher.t>,
  closed?: array<Matcher.t>,
  clicked?: array<Matcher.t>,
}

module Bindings = {
  module SingleDayPicker = {
    @module("react-day-picker") @react.component
    external make: (
      ~mode: mode,
      ~selected: Js.Date.t=?,
      ~onSelect: (option<Js.Date.t>, Js.Date.t) => unit=?,
      ~formatters: formatters=?,
      ~modifiers: modifiers=?,
      ~modifiersClassNames: modifiersClassNames=?,
      ~locale: DateFns.dateFnsLocale=?,
      ~defaultMonth: Js.Date.t=?,
      ~numberOfMonths: int=?,
      ~showOutsideDays: bool=?,
      ~className: string=?,
      ~placeholder: string=?,
      ~footer: React.element=?,
    ) => React.element = "DayPicker"
  }
  module RangeDayPicker = {
    @module("react-day-picker") @react.component
    external make: (
      ~mode: mode,
      ~selected: range=?,
      ~onSelect: (option<range>, Js.Date.t) => unit=?,
      ~formatters: formatters=?,
      ~modifiers: modifiers=?,
      ~modifiersClassNames: modifiersClassNames=?,
      ~locale: DateFns.dateFnsLocale=?,
      ~numberOfMonths: int=?,
      ~defaultMonth: Js.Date.t=?,
      ~showOutsideDays: bool=?,
      ~className: string=?,
      ~placeholder: string=?,
      ~footer: React.element=?,
    ) => React.element = "DayPicker"
  }
}

let applyHoursToRange = (~rangeToUpdate, ~hoursToApply) => {
  Some({
    from: ?(
      rangeToUpdate
      ->Option.flatMap(e => e.from)
      ->Option.map(fromToUpdate =>
        hoursToApply
        ->Option.flatMap(e => e.from)
        ->Option.mapWithDefault(fromToUpdate, fromToApply => {
          fromToUpdate
          ->Js.Date.setHoursM(~hours=fromToApply->Js.Date.getHours, ~minutes=0., ())
          ->Js.Date.fromFloat
        })
      )
    ),
    to: ?(
      rangeToUpdate
      ->Option.flatMap(e => e.to)
      ->Option.map(toToUpdate =>
        hoursToApply
        ->Option.flatMap(e => e.to)
        ->Option.mapWithDefault(toToUpdate, totoApply => {
          toToUpdate
          ->Js.Date.setHoursM(~hours=totoApply->Js.Date.getHours, ~minutes=0., ())
          ->Js.Date.fromFloat
        })
      )
    ),
  })
}

// onSelect callback parameters :
//      #1: selectedDay (computed by library) can be empty
//      #2: clickedDay (direct output onDayClick) mandatory
module SingleDayPicker = {
  @react.component
  let make = (
    ~selected: Js.Date.t=?,
    ~onSelect: (option<Js.Date.t>, Js.Date.t) => unit=?,
    ~formatters: formatters=?,
    ~modifiers: modifiers=?,
    ~modifiersClassNames: modifiersClassNames={},
    ~numberOfMonths: int=?,
    ~defaultMonth: Js.Date.t=?,
    ~showOutsideDays: bool=true,
    ~className: string=?,
    ~placeholder: string=?,
    ~footer: React.element=?,
  ) => {
    let locale = ReactIntl.useIntl()

    <Bindings.SingleDayPicker
      {...%raw(`props`)}
      mode=#single
      showOutsideDays
      locale={locale->ReactIntl.Intl.locale === "fr" ? DateFns.frLocale : DateFns.enLocale}
      modifiersClassNames={
        ...modifiersClassNames,
        closed: cx(["rdp-day_closed", modifiersClassNames.closed->Option.getWithDefault("")]),
      }
    />
  }
}

// onSelect callback parameters :
//      #1: selectedDay (computed by library) can be empty
//      #2: clickedDay (direct output onDayClick) mandatory
module RangeDayPicker = {
  @react.component
  let make = (
    ~selected: range=?,
    ~onSelect: (option<range>, Js.Date.t) => unit=?,
    ~formatters: formatters=?,
    ~modifiers: modifiers=?,
    ~modifiersClassNames: modifiersClassNames={},
    ~numberOfMonths: int=?,
    ~defaultMonth: Js.Date.t=?,
    ~showOutsideDays: bool=true,
    ~className: string=?,
    ~placeholder: string=?,
    ~footer: React.element=?,
  ) => {
    let locale = ReactIntl.useIntl()

    <Bindings.RangeDayPicker
      {...%raw(`props`)}
      mode=#range
      showOutsideDays
      locale={locale->ReactIntl.Intl.locale === "fr" ? DateFns.frLocale : DateFns.enLocale}
      modifiersClassNames={
        ...modifiersClassNames,
        closed: cx(["rdp-day_closed", modifiersClassNames.closed->Option.getWithDefault("")]),
      }
    />
  }
}

module SingleDayPickerInput = {
  @react.component
  let make = (
    ~labelFormatter: Js.Date.t => React.element=?,
    ~labelClassName: string="",
    ~dropdownClassName: string="",
    ~buttonClassName: string="",
    ~placeholder: React.element=?,
    ~value: Js.Date.t=?,
    ~onChange: option<Js.Date.t> => unit,
    ~resetButton=false,
    ~allowEmpty=false,
    ~modifiers=?,
    ~modifiersClassNames=?,
    ~showOutsideDays: bool=true,
    ~defaultMonth: Js.Date.t=?,
  ) => {
    let (validatedValue, setValidatedValue) = React.useState(() => value)
    let (localValue, setLocalValue) = React.useState(() => validatedValue)

    React.useEffect(() => {
      setLocalValue(_ => value)
      setValidatedValue(_ => value)
      None
    }, [value])

    let labelFormatter =
      labelFormatter->Option.getWithDefault(date =>
        <ReactIntl.FormattedDate value={date} day=#"2-digit" month=#"2-digit" year=#numeric />
      )

    <Toolkit__Ui_PortalDropdown
      dropdownClassName
      buttonClassName={cx(["min-w-[100px]", buttonClassName])}
      label={<p className=labelClassName>
        {validatedValue->Option.mapWithDefault(
          placeholder->Option.getWithDefault("-"->React.string),
          labelFormatter,
        )}
      </p>}>
      {disclosure => {
        <React.Fragment>
          <SingleDayPicker
            ?modifiers
            ?modifiersClassNames
            ?defaultMonth
            selected=?{localValue}
            onSelect={(date, _) => setLocalValue(_ => date)}
            showOutsideDays
          />
          <div className="flex justify-end gap-2">
            {resetButton
              ? <Toolkit__Ui_Button
                  onClick={_ => {
                    setLocalValue(_ => None)
                    setValidatedValue(_ => None)
                    onChange(None)
                    disclosure.hide()
                  }}>
                  <ReactIntl.FormattedMessage defaultMessage="Réinitialiser" />
                </Toolkit__Ui_Button>
              : React.null}
            <Toolkit__Ui_Button
              onClick={_ => {
                setLocalValue(_ => validatedValue)
                disclosure.hide()
              }}>
              <ReactIntl.FormattedMessage defaultMessage="Annuler" />
            </Toolkit__Ui_Button>
            <Toolkit__Ui_Button
              color=#success
              disabled={localValue->Option.isNone}
              onClick={_ => {
                setValidatedValue(_ => localValue)
                onChange(localValue)
                disclosure.hide()
              }}>
              <ReactIntl.FormattedMessage defaultMessage="Valider" />
            </Toolkit__Ui_Button>
          </div>
        </React.Fragment>
      }}
    </Toolkit__Ui_PortalDropdown>
  }
}
module RangeDayPickerInput = {
  let hours = Array.make(24, "")->Array.mapWithIndex((index, _) => {
    let prefix = switch index {
    | i if i < 10 => "0"
    | _ => ""
    }

    {
      Toolkit__Ui_Select.label: `${prefix}${index->Int.toString}:00`,
      Toolkit__Ui_Select.value: index->Int.toString,
    }
  })

  module PresetRanges = {
    let now = Js.Date.make()
    let currentDayRange: range = {
      from: now->DateFns.startOfDay,
      to: now->DateFns.endOfDay,
    }
    let currentWeekRange: range = {
      from: now->DateFns.startOfWeek,
      to: now->DateFns.endOfWeek,
    }
    let currentMonthRange: range = {
      from: now->DateFns.startOfMonth,
      to: now->DateFns.endOfMonth,
    }
  }

  type state = {
    range: option<range>,
    startHour: float,
    endHour: float,
  }

  type action =
    | UpdateRange(option<range>)
    | UpdateStartHour(float)
    | UpdateEndHour(float)
    | ResetRange

  @react.component
  let make = (
    ~labelFormatter: range => React.element=?,
    ~labelClassName="",
    ~dropdownClassName="",
    ~buttonClassName="",
    ~value: option<range>=?,
    ~onChange: option<range> => unit,
    ~placeholder=?,
    ~withHours=false,
    ~resetButton=false,
    ~allowEmpty=false,
    ~withPresetRanges=false,
    ~showOutsideDays: bool=true,
    ~modifiers: modifiers=?,
    ~modifiersClassNames: modifiersClassNames=?,
    ~defaultMonth: Js.Date.t=?,
  ) => {
    let previousValue = Toolkit__Hooks.usePrevious(value)

    let (state, dispatch) = ReactUpdate.useReducerWithMapState(
      (state, action) =>
        switch action {
        | UpdateRange(range) =>
          UpdateWithSideEffects(
            {...state, range},
            ({state, send}) => {
              if withHours {
                send(UpdateStartHour(state.startHour))
                send(UpdateEndHour(state.endHour))
              }
              None
            },
          )
        | ResetRange =>
          Update({
            ...state,
            range: None,
          })
        | UpdateStartHour(startHour) =>
          Update({
            ...state,
            startHour,
            range: Some({
              to: ?state.range->Option.flatMap(v => v.to),
              from: ?(
                state.range
                ->Option.flatMap(v => v.from)
                ->Option.map(v =>
                  v->Js.Date.setHoursM(~hours=startHour, ~minutes=0., ())->Js.Date.fromFloat
                )
              ),
            }),
          })
        | UpdateEndHour(endHour) =>
          Update({
            ...state,
            endHour,
            range: Some({
              from: ?state.range->Option.flatMap(v => v.from),
              to: ?(
                state.range
                ->Option.flatMap(v => v.to)
                ->Option.map(v =>
                  v->Js.Date.setHoursM(~hours=endHour, ~minutes=0., ())->Js.Date.fromFloat
                )
              ),
            }),
          })
        },
      () => {
        range: value,
        startHour: value
        ->Option.flatMap(v => v.from)
        ->Option.mapWithDefault(6., v => v->Js.Date.getHours),
        endHour: value
        ->Option.flatMap(v => v.to)
        ->Option.mapWithDefault(23., v => v->Js.Date.getHours),
      },
    )

    React.useEffect(() => {
      dispatch(UpdateRange(value))
      None
    }, [value])

    let labelFormatter = labelFormatter->Option.getWithDefault(range =>
      <div>
        <p className="text-xs leading-4">
          {range.from->Option.mapWithDefault("-"->React.string, date => {
            <ReactIntl.FormattedDate
              value={date}
              day=#"2-digit"
              month=#"2-digit"
              year=#numeric
              hour=?{withHours ? Some(#"2-digit") : None}
              minute=?{withHours ? Some(#"2-digit") : None}
            />
          })}
        </p>
        <p className="text-xs leading-4">
          {range.to->Option.mapWithDefault("-"->React.string, date => {
            <ReactIntl.FormattedDate
              value={date}
              day=#"2-digit"
              month=#"2-digit"
              year=#numeric
              hour=?{withHours ? Some(#"2-digit") : None}
              minute=?{withHours ? Some(#"2-digit") : None}
            />
          })}
        </p>
      </div>
    )

    <Toolkit__Ui_PortalDropdown
      dropdownClassName
      buttonClassName={cx(["min-w-[100px] h-[38px]", buttonClassName])}
      label={<div className={cx(["absolute", labelClassName])}>
        {previousValue
        ->Option.flatMap(v => v)
        ->Option.mapWithDefault(
          placeholder->Option.getWithDefault("-"->React.string),
          labelFormatter,
        )}
      </div>}>
      {disclosure => {
        <div className="flex flex-col items-center">
          {withPresetRanges
            ? <div className="w-full flex flex-col pb-2 border-b-2 mb-2">
                <div
                  className="flex flex-row flex-nowrap items-center justify-start pb-2 border-b mb-2">
                  <Toolkit__Ui_Button
                    onClick={_ => dispatch(UpdateRange(Some(PresetRanges.currentDayRange)))}
                    size=#sm
                    color=#primary
                    variant=#outline
                    className="w-1/2">
                    <ReactIntl.FormattedMessage defaultMessage="Aujourd'hui" />
                  </Toolkit__Ui_Button>
                  <p className="ml-4">
                    <ReactIntl.FormattedDate
                      value={Js.Date.make()} day=#"2-digit" month=#"2-digit" year=#numeric
                    />
                  </p>
                </div>
                <div
                  className="flex flex-row flex-nowrap pb-2 border-b mb-2 items-center justify-start">
                  <Toolkit__Ui_Button
                    onClick={_ => dispatch(UpdateRange(Some(PresetRanges.currentWeekRange)))}
                    size=#sm
                    color=#primary
                    variant=#outline
                    className="w-1/2">
                    <ReactIntl.FormattedMessage defaultMessage="Cette semaine" />
                  </Toolkit__Ui_Button>
                  <p className="ml-4">
                    <ReactIntl.FormattedMessage
                      defaultMessage="Semaine {number}"
                      values={"number": DateFns.getWeekNumber(Js.Date.make())}
                    />
                  </p>
                </div>
                <div className="flex flex-row flex-nowrap items-center justify-start">
                  <Toolkit__Ui_Button
                    onClick={_ => dispatch(UpdateRange(Some(PresetRanges.currentMonthRange)))}
                    size=#sm
                    color=#primary
                    variant=#outline
                    className="w-1/2">
                    <ReactIntl.FormattedMessage defaultMessage="Ce mois" />
                  </Toolkit__Ui_Button>
                  <p className="ml-4 capitalize">
                    <ReactIntl.FormattedDate value={Js.Date.make()} month=#long />
                  </p>
                </div>
              </div>
            : React.null}
          {withHours
            ? <div className="flex flex-row flex-nowrap justify-between px-2 gap-2">
                {state.range
                ->Option.flatMap(e => e.from)
                ->Option.mapWithDefault(React.null, from =>
                  <div className="flex flex-row items-center">
                    <div className="mr-2">
                      <p>
                        <ReactIntl.FormattedMessage defaultMessage="Début" />
                      </p>
                      <p className="text-xs">
                        <ReactIntl.FormattedDate
                          value=from day=#"2-digit" month=#"2-digit" year=#numeric
                        />
                      </p>
                    </div>
                    <Toolkit__Ui_Select
                      value={from->Js.Date.getHours->Float.toString}
                      onChange={value => {
                        dispatch(UpdateStartHour(value->Float.fromString->Option.getExn))
                      }}
                      options=hours
                    />
                  </div>
                )}
                {state.range
                ->Option.flatMap(e => e.to)
                ->Option.mapWithDefault(React.null, to =>
                  <div className="flex flex-row items-center">
                    <div className="mr-2">
                      <p>
                        <ReactIntl.FormattedMessage defaultMessage="Fin" />
                      </p>
                      <p className="text-xs">
                        <ReactIntl.FormattedDate
                          value=to day=#"2-digit" month=#"2-digit" year=#numeric
                        />
                      </p>
                    </div>
                    <Toolkit__Ui_Select
                      value={to->Js.Date.getHours->Float.toString}
                      onChange={value => {
                        dispatch(UpdateEndHour(value->Float.fromString->Option.getExn))
                      }}
                      options=hours
                    />
                  </div>
                )}
              </div>
            : React.null}
          <RangeDayPicker
            ?modifiers
            ?modifiersClassNames
            ?defaultMonth
            selected=?{state.range}
            onSelect={(newValue, _) => {
              dispatch(UpdateRange(newValue))
            }}
            showOutsideDays
          />
          <div className="w-full flex justify-end gap-2">
            {resetButton
              ? <Toolkit__Ui_Button
                  onClick={_ => {
                    dispatch(ResetRange)
                    onChange(None)
                    disclosure.hide()
                  }}>
                  <ReactIntl.FormattedMessage defaultMessage="Réinitialiser" />
                </Toolkit__Ui_Button>
              : React.null}
            <Toolkit__Ui_Button
              onClick={_ => {
                disclosure.hide()
              }}>
              <ReactIntl.FormattedMessage defaultMessage="Annuler" />
            </Toolkit__Ui_Button>
            <Toolkit__Ui_Button
              color=#success
              disabled={switch state.range {
              | _ if allowEmpty => false
              | Some({from: ?None})
              | Some({to: ?None}) => true
              | _ => false
              }}
              onClick={_ => {
                onChange(state.range)
                disclosure.hide()
              }}>
              <ReactIntl.FormattedMessage defaultMessage="Valider" />
            </Toolkit__Ui_Button>
          </div>
        </div>
      }}
    </Toolkit__Ui_PortalDropdown>
  }
}
