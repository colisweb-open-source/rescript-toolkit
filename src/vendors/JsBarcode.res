type barcodeOptions = {
  format?: string,
  height?: float,
  fontSize?: float,
  background?: string,
}
@module("jsbarcode")
external generate: (string, string, barcodeOptions) => unit = "default"
