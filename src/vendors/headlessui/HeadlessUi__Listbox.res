type renderProps = {
  @as("open")
  isOpen: bool,
}

type selectOption<'value> = {
  label: string,
  value: 'value,
  disabled?: bool,
}

@module("@headlessui/react") @react.component
external make: (
  ~children: renderProps => React.element,
  ~value: 'value,
  ~defaultValue: selectOption<'value>=?,
  ~onChange: selectOption<'value> => unit=?,
  ~disabled: bool=?,
  ~by: 'a=?,
  ~name: string=?,
  ~className: string=?,
) => React.element = "Listbox"

module Button = {
  type renderProps<'value> = {value: option<selectOption<'value>>}

  @module("@headlessui/react") @scope("Listbox") @react.component
  external make: (
    ~children: renderProps<'value> => React.element,
    ~className: string=?,
  ) => React.element = "Button"
}
module Options = {
  @module("@headlessui/react") @scope("Listbox") @react.component
  external make: (
    ~children: React.element,
    ~className: string=?,
    ~static: bool=?,
  ) => React.element = "Options"
}
module Option = {
  type renderProps = {
    active: bool,
    selected: bool,
  }

  @module("@headlessui/react") @scope("Listbox") @react.component
  external make: (
    ~children: renderProps => React.element,
    ~value: 'value=?,
    ~disabled: bool=?,
    ~className: string=?,
  ) => React.element = "Option"
}
