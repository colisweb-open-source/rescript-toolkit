module Coordinates = Toolkit.Decoders.Coordinates

// See https://react-google-maps-api-docs.netlify.com/
type gmapInstance

module LatLngBounds = {
  type t
  type paddingClass = {
    left: int,
    right: int,
    top: int,
    bottom: int,
  }

  @new external makeBounds: unit => t = "google.maps.LatLngBounds"

  @send external extend: (t, Coordinates.gmaps) => unit = "extend"

  @send
  external fitBounds: (gmapInstance, t, option<paddingClass>) => unit = "fitBounds"
}

module LoadScript = {
  type t = {
    googleMapsApiKey: string,
    libraries?: array<string>,
  }

  type useLoadScriptReturn = {
    isLoaded: bool,
    loadError: bool,
  }

  @module("@react-google-maps/api")
  external useLoadScript: t => useLoadScriptReturn = "useLoadScript"
}

module GoogleMap = {
  type options
  @obj
  external makeOptions: (
    ~backgroundColor: string=?,
    ~disableDefaultUI: bool=?,
    ~keyboardShortcuts: bool=?,
    ~mapTypeControl: bool=?,
    unit,
  ) => options = ""

  @module("@react-google-maps/api") @react.component
  external make: (
    ~id: string=?,
    ~center: Coordinates.gmaps=?,
    ~zoom: int=?,
    ~mapContainerClassName: string=?,
    ~mapContainerStyle: ReactDOMStyle.t=?,
    ~clickableIcons: bool=?,
    ~options: options=?,
    ~onLoad: gmapInstance => unit=?,
    ~onTilesLoaded: unit => unit=?,
    ~children: React.element=?,
  ) => React.element = "GoogleMap"
}

module SizeClass = {
  type t
  @new external make: (int, int) => t = "google.maps.Size"
}

module MarkerClusterer = {
  type t
  type markerT = {title: string}

  type clusterStyle = {
    url: string,
    height: int,
    width: int,
    anchorText?: (int, int), // (yOffset, xOffset)
    anchorIcon?: (int, int), // (yOffset, xOffset)
    textColor?: string,
    textSize?: int, // in px
    // css properties
    textDecoration?: string, // value of the textDecoration css property
    fontWeight?: string, // value of the fontWeight css property
    fontStyle?: string, // value of the fontStyle css property
    lineHeight?: string,
    fontFamily?: string, // value of the fontFamily css property
    backgroundPosition?: string, // value of the backgroundPosition css property
  }

  type calculatorResult = {
    text?: string,
    index: int, // index of the cluster style you want to use
    title: string,
    html?: string,
  }

  @module("@react-google-maps/api") @react.component
  external make: (
    ~children: t => React.element,
    ~enableRetinaIcons: bool=?,
    ~calculator: (array<markerT>, 'a) => calculatorResult,
    ~styles: array<clusterStyle>=?,
    ~averageCenter: bool=?,
    ~gridSize: int=?,
    ~minimumClusterSize: int=?,
  ) => React.element = "MarkerClusterer"
}

module PinView = {
  type markerContent
  type t = {element: markerContent}

  type params = {
    background: string,
    borderColor: string,
    glyphColor: string,
  }
  @new external make: params => t = "google.maps.marker.PinView"
}
module Marker = {
  type options = {}
  @module("@react-google-maps/api") @react.component
  external make: (
    ~position: Coordinates.gmaps=?,
    ~clickable: bool=?,
    ~draggable: bool=?,
    ~icon: string=?,
    ~label: string=?,
    ~title: string=?,
    ~options: options=?,
    ~clusterer: MarkerClusterer.t=?,
    ~onClick: unit => unit=?,
    ~customValue: string=?,
    ~children: React.element=?,
  ) => React.element = "Marker"
}

module InfoWindow = {
  type options
  @obj external options: (~pixelOffset: int=?) => options = ""

  @module("@react-google-maps/api") @react.component
  external make: (
    ~position: Coordinates.gmaps=?,
    ~children: React.element=?,
    ~onCloseClick: unit => unit=?,
    ~options: options=?,
  ) => React.element = "InfoWindow"
}

module Waypoint = {
  type t = {
    location: Coordinates.gmaps,
    stopover: bool,
  }
  let make = (~lat: float, ~lng: float, ~stopover=true, ()): t => {
    location: Coordinates.make(~lat, ~lng)->Coordinates.toGmaps,
    stopover,
  }
  let areEquals = (a: t, b: t): bool =>
    a.location.lat === b.location.lat && a.location.lng === b.location.lng
}

module DirectionsService = {
  module Options = {
    type t

    type travelMode = [#DRIVING | #BICYCLING | #TRANSIT | #WALKING]

    @obj
    external make: (
      ~destination: Coordinates.gmaps,
      ~origin: Coordinates.gmaps,
      ~travelMode: travelMode,
      ~waypoints: array<Waypoint.t>=?,
      unit,
    ) => t = ""
  }

  module Response = {
    type t

    @get external status: t => string = "status"

    @get @scope(("request", "origin"))
    external _origin: t => 'dest = "location"
    let origin = (r: t): Coordinates.gmaps => {
      lat: (r->_origin)["lat"](),
      lng: (r->_origin)["lng"](),
    }

    @get @scope(("request", "destination"))
    external _destination: t => 'dest = "location"
    let destination = (r: t): Coordinates.gmaps => {
      lat: (r->_destination)["lat"](),
      lng: (r->_destination)["lng"](),
    }

    type rec routes = array<route>
    and route = {legs: array<leg>}
    and leg = {distance: distance}
    and distance = {value: float}
    @get external _routes: t => routes = "routes"
    let route0totalDistance = (r: t) =>
      r
      ->_routes
      ->Array.get(0)
      ->Option.flatMap(route =>
        route.legs
        ->Array.reduce(None, (red, leg) =>
          red->Option.mapWithDefault(
            Some(leg.distance.value),
            red => Some(red +. leg.distance.value),
          )
        )
        ->Option.map(totalDist => #m(totalDist))
      )
  }

  @module("@react-google-maps/api") @react.component
  external make: (
    ~options: Options.t=?,
    ~callback: Js.Nullable.t<Response.t> => unit,
  ) => React.element = "DirectionsService"
}

module DirectionsRenderer = {
  module Options = {
    type t

    @obj
    external make: (
      ~directions: DirectionsService.Response.t,
      ~suppressMarkers: bool=?,
      ~preserveViewport: bool=?,
      unit,
    ) => t = ""
  }

  @module("@react-google-maps/api") @react.component
  external make: (~options: Options.t) => React.element = "DirectionsRenderer"
}
