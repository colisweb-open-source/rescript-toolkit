type onPageChange = {selected: int}

@module("react-paginate") @react.component
external make: (
  ~pageCount: int,
  ~initialPage: int=?,
  ~disableInitialCallback: bool=?,
  ~pageRangeDisplayed: int,
  ~marginPagesDisplayed: int,
  ~previousLabel: React.element=?,
  ~nextLabel: React.element=?,
  ~onPageChange: onPageChange => unit,
  ~pageClassName: string=?,
  ~pageLinkClassName: string=?,
  ~activeClassName: string=?,
  ~activeLinkClassName: string=?,
  ~previousClassName: string=?,
  ~nextClassName: string=?,
  ~previousLinkClassName: string=?,
  ~nextLinkClassName: string=?,
  ~disabledClassName: string=?,
  ~containerClassName: string=?,
) => React.element = "default"
