@val
external cmd2: (string, string) => unit = "window.zE"
@val
external cmd3: (string, string, 'a) => unit = "window.zE"

type onLoadCallback = unit => unit
type key = string

let load: (key, onLoadCallback) => unit = %raw(`
  function (key, cb) {
    var d = document;
    var s = d.createElement('script');
    s.id = 'ze-snippet';
    s.src = 'https://static.zdassets.com/ekr/snippet.js?key='+ key;
    s.async = 1;
    s.addEventListener("load", cb);
    d.getElementsByTagName('head')[0].appendChild(s);
  }
`)

let use = (key: key, onLoad: onLoadCallback) => {
  let isInit = React.useRef(false)

  React.useEffect(() => {
    if !isInit.current {
      load(key, onLoad)

      isInit.current = true
    }
    None
  }, (key, onLoad))
}
