@val
external describe: (string, @uncurry (unit => unit)) => unit = "describe"

@val
external test: (string, @uncurry (unit => unit)) => unit = "test"

type expect

@val external expect: 'a => expect = "expect"

@get external not__: expect => expect = "not"

@send external toEqual: (expect, 'a) => 'b = "toEqual"
@send external toEqual0: (expect, 'a) => unit = "toEqual"
@send external toBe: (expect, 'a) => unit = "toBe"

@send external toContainObject: (expect, 'a) => unit = "toContainObject"

@val external expectArrayContaining: 'a => 'b = "expect.arrayContaining"
@val
external expectObjectContaining: 'a => 'b = "expect.objectContaining"
@val
external expectObjectNotContaining: 'a => 'b = "expect.not.objectContaining"
