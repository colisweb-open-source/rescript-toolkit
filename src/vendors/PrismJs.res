module Language = {
  type t

  @module("prismjs") @scope("languages")
  external javascript: t = "javascript"
}

@module("prismjs")
external highlight: (string, Language.t, string) => string = "highlight"
