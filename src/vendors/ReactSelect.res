@ocaml.doc("
 * Bindings of https://www.npmjs.com/package/react-select
 ")
type optionMessage = {inputValue: string}

type rec labelOption<'value> = {
  label: string,
  value: 'value,
  options?: array<labelOption<'value>>,
}

type optionMessageCb = {inputValue: string}

module ReactSelect = {
  @module("react-select") @react.component
  external make: (
    ~defaultMenuIsOpen: bool=?,
    ~name: string,
    ~autoFocus: bool=?,
    ~isMulti: bool=?,
    ~isDisabled: bool=?,
    ~isLoading: bool=?,
    ~isSearchable: bool=?,
    ~isClearable: bool=?,
    ~placeholder: string=?,
    ~className: string=?,
    ~classNamePrefix: string=?,
    ~value: labelOption<'value>=?,
    ~defaultValue: labelOption<'value>=?,
    ~onChange: labelOption<'value> => unit,
    ~options: array<labelOption<'value>>,
    ~noOptionsMessage: option<optionMessage => Js.Nullable.t<string>>=?,
  ) => React.element = "default"
}

module ReactSelectMultiple = {
  module ReactSelect = {
    @module("react-select") @react.component
    external make: (
      ~name: string,
      ~defaultMenuIsOpen: bool=?,
      ~autoFocus: bool=?,
      ~isMulti: bool=?,
      ~isDisabled: bool=?,
      ~isLoading: bool=?,
      ~isSearchable: bool=?,
      ~placeholder: string=?,
      ~className: string=?,
      ~classNamePrefix: string=?,
      ~defaultValue: array<labelOption<'value>>=?,
      ~value: array<labelOption<'value>>=?,
      ~onChange: array<labelOption<'value>> => unit,
      ~options: array<labelOption<'value>>,
      ~noOptionsMessage: optionMessage => Js.Nullable.t<string>=?,
    ) => React.element = "default"
  }

  @react.component
  let make = (
    ~name: string,
    ~autoFocus: option<bool>=?,
    ~isSearchable: option<bool>=?,
    ~defaultMenuIsOpen: option<bool>=?,
    ~isDisabled: option<bool>=?,
    ~isLoading: option<bool>=?,
    ~placeholder: option<string>=?,
    ~className: option<string>=?,
    ~classNamePrefix: option<string>=?,
    ~defaultValue: option<array<labelOption<'value>>>=?,
    ~value: option<array<labelOption<'value>>>=?,
    ~onChange: array<labelOption<'value>> => unit,
    ~options: array<labelOption<'value>>,
    ~noOptionsMessage: option<optionMessage => Js.Nullable.t<string>>=?,
  ) =>
    <ReactSelect
      name
      ?autoFocus
      ?isLoading
      ?isDisabled
      isMulti=true
      ?isSearchable
      ?placeholder
      ?defaultMenuIsOpen
      ?className
      ?classNamePrefix
      ?value
      ?defaultValue
      onChange
      options
      ?noOptionsMessage
    />
}

module ReactAsyncSelect = {
  @module("react-select/async") @react.component
  external make: (
    ~name: string,
    ~autoFocus: bool=?,
    ~isMulti: bool=?,
    ~isDisabled: bool=?,
    ~isLoading: bool=?,
    ~isSearchable: bool=?,
    ~placeholder: string=?,
    ~noOptionsMessage: optionMessageCb => string=?,
    ~loadingMessage: optionMessageCb => string=?,
    ~className: string=?,
    ~classNamePrefix: string=?,
    ~value: labelOption<'value>=?,
    ~defaultValue: option<'value>=?,
    ~onChange: labelOption<'value> => unit,
    ~cacheOptions: bool=?,
    ~defaultOptions: option<array<labelOption<'value>>>=?,
    ~loadOptions: string => Promise.Js.t<array<labelOption<'value>>, string>,
  ) => React.element = "default"
}

module ReactAsyncSelectMultiple = {
  @module("react-select/async") @react.component
  external make: (
    ~name: string,
    ~autoFocus: bool=?,
    ~isMulti: bool=?,
    ~isDisabled: bool=?,
    ~isLoading: bool=?,
    ~isSearchable: bool=?,
    ~placeholder: string=?,
    ~noOptionsMessage: optionMessageCb => string=?,
    ~loadingMessage: optionMessageCb => string=?,
    ~className: string=?,
    ~classNamePrefix: string=?,
    ~value: option<array<labelOption<'value>>>=?,
    ~defaultValue: option<'value>=?,
    ~onChange: array<labelOption<'value>> => unit,
    ~cacheOptions: bool=?,
    ~defaultOptions: option<array<labelOption<'value>>>=?,
    ~loadOptions: string => Promise.Js.t<array<labelOption<'value>>, string>,
  ) => React.element = "default"
}

module ReactSelectMultipleCreateInput = {
  type labelOption<'value> = {
    label: string,
    value: 'value,
  }
  module ReactSelect = {
    type components = {"DropdownIndicator": React.element}
    type labeledValue = {value: string, label: string}
    @module("react-select/creatable") @react.component
    external make: (
      ~components: components,
      ~inputValue: string,
      ~isClearable: bool=?,
      ~isMulti: bool=?,
      ~menuIsOpen: bool=?,
      ~onChange: array<labelOption<'a>> => unit,
      ~onInputChange: string => unit,
      ~onKeyDown: ReactEvent.Keyboard.t => unit,
      ~placeholder: string=?,
      ~value: array<labelOption<'a>>,
      ~isDisabled: bool=?,
      ~isLoading: bool=?,
      ~className: string=?,
      ~id: string=?,
    ) => React.element = "default"
  }

  @react.component
  let make = (
    ~value: array<string>,
    ~onChange: array<string> => unit,
    ~isClearable=true,
    ~placeholder="",
    ~isDisabled=false,
    ~isLoading=false,
    ~className: option<string>=?,
    ~id: option<string>=?,
  ) => {
    let (inputValue, onInputChange) = React.useState(() => "")
    let onInputChange = v => onInputChange(_ => v)

    <ReactSelect
      components={"DropdownIndicator": React.null}
      isMulti=true
      onKeyDown={e => {
        if inputValue === "" {
          ()
        } else {
          switch e->ReactEvent.Keyboard.key {
          | "Enter"
          | "Tab" => {
              onInputChange("")
              onChange(value->Array.concat([inputValue]))
              e->ReactEvent.Keyboard.preventDefault
            }

          | _ => ()
          }
        }
      }}
      menuIsOpen={false}
      inputValue
      isClearable
      onChange={values => onChange(values->Array.map(({label}) => label))}
      onInputChange
      placeholder
      value={value->Array.mapWithIndex((i, v) => {label: v, value: v ++ i->Js.Int.toString})}
      isDisabled
      isLoading
      ?className
      ?id
    />
  }
}
