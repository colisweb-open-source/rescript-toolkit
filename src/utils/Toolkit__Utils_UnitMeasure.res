module Weight = {
  module Unit = {
    @spice
    type t =
      | @spice.as("g") Gram
      | @spice.as("kg") Kg
  }

  @spice
  type t = [#g(float) | #kg(float)]

  let wrapValue = (unit: Unit.t, value) => {
    switch unit {
    | Gram => #g(value)
    | Kg => #kg(value)
    }
  }

  let toValue = value =>
    switch value {
    | #g(v)
    | #kg(v) => v
    }

  let display = (value, ~digits=2, ()) =>
    switch value {
    | #g(v) => v->Js.Float.toFixedWithPrecision(~digits) ++ " g"
    | #kg(v) => v->Js.Float.toFixedWithPrecision(~digits) ++ " kg"
    }
  let toString = value =>
    switch value {
    | #g(v) => v->Float.toString ++ " g"
    | #kg(v) => v->Float.toString ++ " kg"
    }

  let makeG = (value: float) => #g(value)
  let makeKg = (value: float) => #kg(value)
}

module Time = {
  module Unit = {
    @spice
    type t =
      | @spice.as("min") Min
      | @spice.as("h") Hour
      | @spice.as("s") Second
  }

  type t = [#min(float) | #h(float) | #s(float)]

  let wrapValue = (unit: Unit.t, value) => {
    switch unit {
    | Min => #min(value)
    | Hour => #h(value)
    | Second => #s(value)
    }
  }

  let toValue = value =>
    switch value {
    | #s(v)
    | #min(v)
    | #h(v) => v
    }

  let toString = value =>
    switch value {
    | #min(v) => v->Float.toString ++ " min"
    | #h(v) => v->Float.toString ++ " h"
    | #s(v) => v->Float.toString ++ " s"
    }

  let display = (value, ~digits as _=2, ()) => {
    let _initialDate = Js.Date.make()

    let duration = switch value {
    | #min(v) =>
      DateFns.intervalToDuration(
        ~interval={
          start: _initialDate,
          end_: _initialDate->DateFns.addMinutes(v),
        },
      )
    | #h(v) =>
      DateFns.intervalToDuration(
        ~interval={start: _initialDate, end_: _initialDate->DateFns.addHours(v)},
      )
    | #s(v) =>
      DateFns.intervalToDuration(
        ~interval={start: _initialDate, end_: _initialDate->DateFns.addSeconds(v)},
      )
    }

    switch (
      duration.years,
      duration.months,
      duration.days,
      duration.hours,
      duration.minutes,
      duration.seconds,
    ) {
    | (None, None, None, Some(h), Some(min), sec) =>
      let secondsAsMinutesRounded = sec->Option.mapWithDefault(0, sec => sec > 30 ? 1 : 0)
      let minRounded =
        (min + secondsAsMinutesRounded === 60 ? 0 : min + secondsAsMinutesRounded)->Int.toString
      let hRounded = min + secondsAsMinutesRounded === 60 ? h + 1 : h

      `${hRounded->Int.toString} : ${minRounded->String.length === 1
          ? "0" ++ minRounded
          : minRounded}`
    | (None, None, None, Some(h), None, _) => `${h->Int.toString} h`
    | (None, None, None, None, Some(min), sec) =>
      let secondsAsMinutesRounded = sec->Option.mapWithDefault(0, sec => sec > 30 ? 1 : 0)
      `${(min + secondsAsMinutesRounded)->Int.toString} min`
    | (None, None, None, None, None, Some(sec)) => `${sec->Int.toString} sec`
    | _ => DateFns.formatDuration(duration)
    }
  }
}

module Dimension = {
  module Unit = {
    @spice
    type t =
      | @spice.as("mm") Mm
      | @spice.as("cm") Cm
      | @spice.as("dm") Dm
      | @spice.as("m") M
      | @spice.as("km") Km
  }

  type t = [
    | #mm(float)
    | #cm(float)
    | #dm(float)
    | #m(float)
    | #km(float)
  ]

  let wrapValue = (unit: Unit.t, value: float): t =>
    switch unit {
    | Mm => #mm(value)
    | Cm => #cm(value)
    | Dm => #dm(value)
    | M => #m(value)
    | Km => #km(value)
    }

  let toValue = value =>
    switch value {
    | #mm(v)
    | #cm(v)
    | #dm(v)
    | #m(v)
    | #km(v) => v
    }

  let display = (value, ~digits=2, ()) => {
    open Js.Float
    switch value {
    | #mm(v) => v->toFixedWithPrecision(~digits) ++ " mm"
    | #cm(v) => v->toFixedWithPrecision(~digits) ++ " cm"
    | #dm(v) => v->toFixedWithPrecision(~digits) ++ " dm"
    | #m(v) => v->toFixedWithPrecision(~digits) ++ " m"
    | #km(v) => v->toFixedWithPrecision(~digits) ++ " km"
    }
  }

  let toString = value => {
    switch value {
    | #mm(v) => v->Float.toString ++ " mm"
    | #cm(v) => v->Float.toString ++ " cm"
    | #dm(v) => v->Float.toString ++ " dm"
    | #m(v) => v->Float.toString ++ " m"
    | #km(v) => v->Float.toString ++ " km"
    }
  }
}

module Speed = {
  module Unit = {
    @spice
    type t = | @spice.as("km/h") Km_h
  }

  type t = [
    | #km_h(float)
  ]

  let wrapValue = (unit: Unit.t, value: float): t =>
    switch unit {
    | Km_h => #km_h(value)
    }

  let toValue = value =>
    switch value {
    | #km_h(v) => v
    }

  let display = (value, ~digits=2, ()) => {
    open Js.Float
    switch value {
    | #km_h(v) => v->toFixedWithPrecision(~digits) ++ " km/h"
    }
  }

  let toString = value => {
    switch value {
    | #km_h(v) => v->Float.toString ++ " km/h"
    }
  }
}

module Volume = {
  module Unit = {
    @spice
    type t = | @spice.as("m³") M3
  }

  type t = [
    | #m3(float)
  ]

  let wrapValue = (unit: Unit.t, value: float): t =>
    switch unit {
    | M3 => #m3(value)
    }

  let toValue = value =>
    switch value {
    | #m3(value) => value
    }

  let display = (value, ~digits=2, ()) => {
    open Js.Float
    switch value {
    | #m3(v) => v->toFixedWithPrecision(~digits) ++ ` m³`
    }
  }

  let toString = value => {
    switch value {
    | #m3(v) => v->Float.toString ++ ` m³`
    }
  }
}

module EnergyCapacity = {
  module Unit = {
    @spice
    type t = | @spice.as("kWh") Kwh
  }

  type t = [
    | #kWh(float)
  ]

  let wrapValue = (unit: Unit.t, value: float): t =>
    switch unit {
    | Kwh => #kWh(value)
    }

  let toValue = value =>
    switch value {
    | #kWh(value) => value
    }

  let display = (value, ~digits=2, ()) => {
    open Js.Float
    switch value {
    | #kWh(v) => v->toFixedWithPrecision(~digits) ++ ` kWh`
    }
  }

  let toString = value => {
    switch value {
    | #kWh(v) => v->Float.toString ++ ` kWh`
    }
  }
}

module Currency = {
  module Unit = {
    @spice
    type t = | @spice.as("EUR") EUR
  }

  type t = [
    | #EUR(float)
  ]

  let wrapValue = (unit: Unit.t, value: float): t =>
    switch unit {
    | EUR => #EUR(value)
    }

  let toValue = value =>
    switch value {
    | #EUR(v) => v
    }

  let toString = value => {
    switch value {
    | #EUR(v) => v->Float.toString ++ " EUR"
    }
  }

  let display = (value, ~digits=2, ()) => {
    switch value {
    | #EUR(v) => v->Js.Float.toFixedWithPrecision(~digits) ++ " EUR"
    }
  }
}

module CompositeUnits = {
  module CurrencyPerDistance = {
    module Unit = {
      @spice
      type t = | @spice.as("EUR/km") EUR_km
    }

    type t = [
      | #EUR_km(float)
    ]

    let wrapValue = (unit: Unit.t, value: float): t =>
      switch unit {
      | EUR_km => #EUR_km(value)
      }

    let toValue = value =>
      switch value {
      | #EUR_km(v) => v
      }

    let toString = value => {
      switch value {
      | #EUR_km(v) => v->Float.toString ++ " EUR/km"
      }
    }

    let display = (value, ~digits=2, ()) => {
      switch value {
      | #EUR_km(v) => v->Js.Float.toFixedWithPrecision(~digits) ++ " EUR/km"
      }
    }
  }

  module CurrencyPerTime = {
    module Unit = {
      @spice
      type t = | @spice.as("EUR/h") EUR_h
    }

    type t = [
      | #EUR_h(float)
    ]

    let wrapValue = (unit: Unit.t, value: float): t =>
      switch unit {
      | EUR_h => #EUR_h(value)
      }

    let toValue = value =>
      switch value {
      | #EUR_h(v) => v
      }

    let toString = value => {
      switch value {
      | #EUR_h(v) => v->Float.toString ++ " EUR/h"
      }
    }

    let display = (value, ~digits=2, ()) => {
      switch value {
      | #EUR_h(v) => v->Js.Float.toFixedWithPrecision(~digits) ++ " EUR/h"
      }
    }
  }
}
