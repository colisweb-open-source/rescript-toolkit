# Utils

Some utilities types

## UnitMeasure

### Dimension

```rescript
let mm = `mm(10.);

mm->Toolkit.Utils.UnitMeasure.Dimension.toString; // "10.00 mm"
```

### Weight

```rescript
let kg = `kg(10.);

kg->Toolkit.Utils.UnitMeasure.Weight.toString; // "10.00 kg"
```
