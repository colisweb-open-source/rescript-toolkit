# Form

## Usage

A form must contains 2 things :

- an error type
- the state of your form with `lenses-ppx`

```rescript
module FormState = {
  type error = ColiswebApi.V5.Store.Contact.CreateContact.Request.error;
  include [%lenses
            type state = {
              firstName: string,
              lastName: string,
              email: string,
              phone1: string,
              phone2: string,
            }
          ];
};

module FormApi = Toolkit.Form.Make(FormState);

[@react.component]
let make = () => {

  let initialState =
    FormState.{
      firstName: "",
      lastName: "",
      phone1: "",
      phone2: "",
      email: "",
    };

  let schema =
    FormApi.Form.Validation.Schema([|
      StringNonEmpty(FirstName),
      StringNonEmpty(LastName),
      Email(Email),
    |]);

  let onSubmit = (form: FormApi.Form.onSubmitAPI) => {
    let {values}: FormApi.Form.state = form.state;

    ColiswebApi.V5.Store.Contact.CreateContact.Config.exec((
      clientId,
      storeId,
      {
        firstName: values.firstName->OptionUtils.fromString,
        lastName: values.lastName->OptionUtils.fromString,
        email: values.email,
        phone1: values.phone1->OptionUtils.fromString,
        phone2: values.phone2->OptionUtils.fromString,
      },
    ))
    ->Promise.flatMapOk(result =>
        reloadContact()->Promise.map(_ => Ok(result))
      )
    ->Promise.tapOk(_ => hide())
    ->Promise.mapError(error => error->Obj.magic /*** TODO: improve */);
  };

  let form = FormApi.Form.use(~initialState, ~schema, ~onSubmit, ());

  <FormApi.Form.Provider value=form>
    <form onSubmit={event => {
        ReactEvent.Synthetic.preventDefault(event);
        form.submit();
      }}>

      <Form.Field
        field
        render={({handleChange, error, value, validate}) => {
          let isInvalid = error->Option.isSome;
          <>
            <input value onChange={BsReform.Helpers.handleChange(handleChange)} />

            {
              error->Option.mapWithDefault(React.null, e => <p>e->React.string</p>)
            }
          </>;
        }}
      />;

      <Button
        type_="submit"
        color=`primary
        isLoading={form.state.formState === Submitting}
        onClick={_ => form.submit()}>
        <FormattedMessage
          id="forms.controls.confirm"
          defaultMessage="Confirm"
        />
      </Button>
    </form>
  </FormApi.Form.Provider>;
}
```
