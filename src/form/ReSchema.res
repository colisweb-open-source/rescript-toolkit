module type Lenses = {
  type field<'a>
  type state
  let set: (state, field<'a>, 'a) => state
  let get: (state, field<'a>) => 'a
}

type childFieldError = {
  error: string,
  index: int,
  name: string,
}

type rec fieldState<'a> =
  | Valid
  | NestedErrors(array<childFieldError>)
  | NestedErrors2(array<array<('a, fieldState<'a>)>>)
  | Error(string)

type recordValidationState<'a> =
  | Valid
  | Errors(array<('a, fieldState<'a>)>)

module Make = (Lenses: Lenses) => {
  type rec field = Field(Lenses.field<'a>): field

  module Validation = {
    type rec t =
      | Email({
          field: Lenses.field<string>,
          optionalPredicate?: Lenses.state => bool,
          error: option<string>,
        }): t
      | Phone({
          field: Lenses.field<string>,
          optionalPredicate?: Lenses.state => bool,
          error: option<string>,
        }): t
      | Password({
          field: Lenses.field<string>,
          login?: Lenses.state => string,
          optionalPredicate?: Lenses.state => bool,
          error: option<string>,
        }): t
      | OptionalPassword({
          field: Lenses.field<string>,
          login?: Lenses.state => string,
          error: option<string>,
        }): t
      | NoValidation({field: Lenses.field<'a>}): t
      | StringNonEmpty({
          field: Lenses.field<string>,
          optionalPredicate?: Lenses.state => bool,
          error: option<string>,
        }): t
      | StringRegExp({field: Lenses.field<string>, matches: string, error: option<string>}): t
      | StringMin({field: Lenses.field<string>, min: int, error: option<string>}): t
      | StringMax({field: Lenses.field<string>, max: int, error: option<string>}): t
      | IntMin({field: Lenses.field<int>, min: int, error: option<string>}): t
      | IntMax({field: Lenses.field<int>, max: int, error: option<string>}): t
      | FloatMin({field: Lenses.field<float>, min: float, error: option<string>}): t
      | FloatMax({field: Lenses.field<float>, max: float, error: option<string>}): t
      | Custom({field: Lenses.field<'a>, predicate: Lenses.state => fieldState<field>}): t
      | CustomNestedSchema({
          field: Lenses.field<'a>,
          predicate: Lenses.state => array<(int, recordValidationState<'a>)>,
        }): t
      | CustomNestedSchema2({
          field: Lenses.field<'a>,
          predicate: Lenses.state => array<array<(field, fieldState<field>)>>,
        }): t
      | True({field: Lenses.field<bool>, error: option<string>}): t
      | False({field: Lenses.field<bool>, error: option<string>}): t
      | OptionNonEmpty({
          field: Lenses.field<option<'a>>,
          optionalPredicate?: Lenses.state => bool,
          error: option<string>,
        }): t
      | ArrayNonEmpty({field: Lenses.field<array<'a>>, error: option<string>}): t

    type schema = array<t>

    let schema = validations => validations->Belt.Array.concatMany

    let mergeValidators = validations => {
      validations->Belt.Array.reduce([], (rules, (rule, apply)) =>
        switch rule {
        | None => rules
        | Some(rule) => rules->Belt.Array.concat([rule->apply])
        }
      )
    }

    let custom = (field, predicate) => [Custom({field, predicate})]

    let customNestedSchema = (field, predicate) => {
      [CustomNestedSchema({field, predicate})]
    }
    let customNestedSchema2 = (field, predicate) => {
      [CustomNestedSchema2({field, predicate})]
    }

    let optionNonEmpty = (~error=?, ~optionalPredicate=?, field) => [
      OptionNonEmpty({field, ?optionalPredicate, error}),
    ]

    let arrayNonEmpty = (~error=?, field) => [ArrayNonEmpty({field, error})]

    let true_ = (~error=?, field) => [True({field, error})]

    let false_ = (~error=?, field) => [False({field, error})]

    let email = (~error=?, ~optionalPredicate=?, field) => [
      Email({field, ?optionalPredicate, error}),
    ]
    let phone = (~error=?, ~optionalPredicate=?, field) => [
      Phone({field, ?optionalPredicate, error}),
    ]
    let password = (~error=?, ~login=?, ~optionalPredicate=?, field) => [
      Password({field, ?login, ?optionalPredicate, error}),
    ]
    let optionalPassword = (~error=?, ~login=?, field) => [OptionalPassword({field, ?login, error})]

    let nonEmpty = (~error=?, ~optionalPredicate=?, field) => [
      StringNonEmpty({field, ?optionalPredicate, error}),
    ]

    let string = (~min=?, ~minError=?, ~max=?, ~maxError=?, field) => {
      mergeValidators([
        (
          min,
          min => StringMin({
            field,
            min,
            error: minError,
          }),
        ),
        (
          max,
          max => StringMax({
            field,
            max,
            error: maxError,
          }),
        ),
      ])
    }

    let regExp = (~error=?, ~matches, field) => [StringRegExp({field, matches, error})]

    let float = (~min=?, ~minError=?, ~max=?, ~maxError=?, field) => {
      mergeValidators([
        (
          min,
          min => FloatMin({
            field,
            min,
            error: minError,
          }),
        ),
        (
          max,
          max => FloatMax({
            field,
            max,
            error: maxError,
          }),
        ),
      ])
    }

    let int = (~min=?, ~minError=?, ~max=?, ~maxError=?, field) => {
      mergeValidators([
        (
          min,
          min => IntMin({
            field,
            min,
            error: minError,
          }),
        ),
        (
          max,
          max => IntMax({
            field,
            max,
            error: maxError,
          }),
        ),
      ])
    }
  }

  let validateField = (~validator, ~values, ~i18n: ReSchemaI18n.t): (field, fieldState<field>) =>
    switch validator {
    | Validation.True({field, error}) =>
      let value = Lenses.get(values, field)
      (Field(field), value ? Valid : Error(error->Belt.Option.getWithDefault(i18n.true_())))
    | Validation.False({field, error}) =>
      let value = Lenses.get(values, field)
      (
        Field(field),
        value == false ? Valid : Error(error->Belt.Option.getWithDefault(i18n.false_())),
      )
    | Validation.IntMin({field, min, error}) =>
      let value = Lenses.get(values, field)
      (
        Field(field),
        value >= min ? Valid : Error(error->Belt.Option.getWithDefault(i18n.intMin(~value, ~min))),
      )
    | Validation.IntMax({field, max, error}) =>
      let value = Lenses.get(values, field)

      (
        Field(field),
        value <= max ? Valid : Error(error->Belt.Option.getWithDefault(i18n.intMax(~value, ~max))),
      )
    | Validation.FloatMin({field, min, error}) =>
      let value = Lenses.get(values, field)
      (
        Field(field),
        value >= min
          ? Valid
          : Error(error->Belt.Option.getWithDefault(i18n.floatMin(~value, ~min))),
      )
    | Validation.FloatMax({field, max, error}) =>
      let value = Lenses.get(values, field)
      (
        Field(field),
        Lenses.get(values, field) <= max
          ? Valid
          : Error(error->Belt.Option.getWithDefault(i18n.floatMax(~value, ~max))),
      )
    | Validation.Email({field, error} as props) =>
      let value = Lenses.get(values, field)
      (
        Field(field),
        switch value {
        | "" if props.optionalPredicate->Option.mapWithDefault(false, fn => fn(values)) => Valid
        | value =>
          Js.Re.test_(ReSchemaRegExp.email, value)
            ? Valid
            : Error(error->Belt.Option.getWithDefault(i18n.email(~value)))
        },
      )
    | Validation.Phone({field, error} as props) =>
      let value = Lenses.get(values, field)
      (
        Field(field),
        switch (value->Obj.magic: option<string>) {
        | _ if props.optionalPredicate->Option.mapWithDefault(false, fn => fn(values)) => Valid
        | None
        | Some("") =>
          Error(i18n.stringNonEmpty(~value=""))
        | Some(phone) =>
          if !(phone->Js.String2.startsWith("+33")) {
            Error(error->Option.getWithDefault(i18n.phone))
          } else if phone->Js.String2.sliceToEnd(~from=3)->Js.String2.length != 9 {
            Error(error->Option.getWithDefault(i18n.phone))
          } else {
            Valid
          }
        },
      )
    | Validation.Password(fields) => {
        let value = Lenses.get(values, fields.field)

        (
          Field(fields.field),
          switch (value, fields.login->Option.map(fn => fn(values))) {
          | _ if fields.optionalPredicate->Option.mapWithDefault(false, fn => fn(values)) => Valid
          | ("", _) => Error(i18n.stringNonEmpty(~value=""))
          | (password, _)
            if !Js.Re.test_(%re("/(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{10,}/"), password) =>
            Error(fields.error->Option.getWithDefault(i18n.passwordTooShort))
          | (p, Some(login))
            if p->Js.String2.toLowerCase->Js.String2.includes(login->Js.String2.toLowerCase) =>
            Error(i18n.passwordCantContainLogin)

          | _ => Valid
          },
        )
      }
    | Validation.OptionalPassword(fields) => {
        let value = Lenses.get(values, fields.field)

        (
          Field(fields.field),
          switch (value, fields.login->Option.map(fn => fn(values))) {
          | ("", _) => Valid
          | (password, _)
            if !Js.Re.test_(%re("/(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{10,}/"), password) =>
            Error(fields.error->Option.getWithDefault(i18n.passwordTooShort))
          | (p, Some(login))
            if p->Js.String2.toLowerCase->Js.String2.includes(login->Js.String2.toLowerCase) =>
            Error(i18n.passwordCantContainLogin)

          | _ => Valid
          },
        )
      }
    | Validation.NoValidation({field}) => (Field(field), Valid)
    | Validation.StringNonEmpty({field, error} as props) =>
      let value = Lenses.get(values, field)
      (
        Field(field),
        switch value {
        | _ if props.optionalPredicate->Option.mapWithDefault(false, fn => fn(values)) => Valid
        | "" => Error(error->Belt.Option.getWithDefault(i18n.stringNonEmpty(~value)))
        | _ => Valid
        },
      )
    | Validation.StringRegExp({field, matches, error}) =>
      let value = Lenses.get(values, field)
      (
        Field(field),
        Js.Re.test_(Js.Re.fromString(matches), value)
          ? Valid
          : Error(error->Belt.Option.getWithDefault(i18n.stringRegExp(~value, ~pattern=matches))),
      )
    | Validation.StringMin({field, min, error}) =>
      let value = Lenses.get(values, field)
      (
        Field(field),
        Js.String.length(value) >= min
          ? Valid
          : Error(error->Belt.Option.getWithDefault(i18n.stringMin(~value, ~min))),
      )
    | Validation.StringMax({field, max, error}) =>
      let value = Lenses.get(values, field)
      (
        Field(field),
        Js.String.length(value) <= max
          ? Valid
          : Error(error->Belt.Option.getWithDefault(i18n.stringMax(~value, ~max))),
      )
    | Validation.Custom({field, predicate}) => (Field(field), predicate(values))
    | Validation.CustomNestedSchema({field, predicate}) => {
        let results = predicate(values)

        let errors =
          results
          ->Array.map(((index, result)) => {
            switch result {
            | Errors(errors) =>
              errors->Array.map(((fieldName, error)) => {
                switch (fieldName->Obj.magic, error) {
                | (Field(name), Error(e)) =>
                  Some({
                    error: e,
                    index,
                    name: name->Obj.magic,
                  })
                | _ => None
                }
              })
            | Valid => []
            }
          })
          ->Array.reduce([], (acc, el) => acc->Array.concat(el))
          ->Array.keepMap(v => v)

        (Field(field)->Obj.magic, errors->Array.length > 0 ? NestedErrors(errors) : Valid)
      }
    | Validation.CustomNestedSchema2({field, predicate}) => {
        let results = predicate(values)

        (Field(field), NestedErrors2(results))
      }
    | Validation.OptionNonEmpty({field, error} as props) => {
        let value = Lenses.get(values, field)

        (
          Field(field),
          switch value {
          | _ if props.optionalPredicate->Option.mapWithDefault(false, fn => fn(values)) => Valid
          | None => Error(error->Belt.Option.getWithDefault(i18n.stringNonEmpty(~value="")))
          | Some(_) => Valid
          },
        )
      }
    | Validation.ArrayNonEmpty({field, error}) => {
        let value = Lenses.get(values, field)

        (
          Field(field),
          value->Array.length == 0
            ? Error(error->Belt.Option.getWithDefault(i18n.stringNonEmpty(~value="")))
            : Valid,
        )
      }
    }

  let getFieldValidator = (~validators, ~fieldName) =>
    validators->Belt.Array.getBy(validator =>
      switch validator {
      | Validation.False({field}) => Field(field) == fieldName
      | Validation.True({field}) => Field(field) == fieldName
      | Validation.IntMin({field}) => Field(field) == fieldName
      | Validation.IntMax({field}) => Field(field) == fieldName
      | Validation.FloatMin({field}) => Field(field) == fieldName
      | Validation.FloatMax({field}) => Field(field) == fieldName
      | Validation.Email({field}) => Field(field) == fieldName
      | Validation.Password({field}) => Field(field) == fieldName
      | Validation.OptionalPassword({field}) => Field(field) == fieldName
      | Validation.NoValidation({field}) => Field(field) == fieldName
      | Validation.StringNonEmpty({field}) => Field(field) == fieldName
      | Validation.StringRegExp({field}) => Field(field) == fieldName
      | Validation.StringMin({field}) => Field(field) == fieldName
      | Validation.StringMax({field}) => Field(field) == fieldName
      | Validation.Custom({field}) => Field(field) == fieldName
      | Validation.CustomNestedSchema({field}) => Field(field) == fieldName
      | Validation.OptionNonEmpty({field}) => Field(field) == fieldName
      | Validation.ArrayNonEmpty({field}) => Field(field) == fieldName
      | Validation.CustomNestedSchema2({field}) => Field(field) == fieldName
      | Validation.Phone({field}) => Field(field) == fieldName
      }
    )

  let getFieldValidators = (~validators, ~fieldName) =>
    validators->Belt.Array.keep(validator =>
      switch validator {
      | Validation.False({field}) => Field(field) == fieldName
      | Validation.Phone({field}) => Field(field) == fieldName
      | Validation.True({field}) => Field(field) == fieldName
      | Validation.Password({field}) => Field(field) == fieldName
      | Validation.OptionalPassword({field}) => Field(field) == fieldName
      | Validation.IntMin({field}) => Field(field) == fieldName
      | Validation.IntMax({field}) => Field(field) == fieldName
      | Validation.FloatMin({field}) => Field(field) == fieldName
      | Validation.FloatMax({field}) => Field(field) == fieldName
      | Validation.Email({field}) => Field(field) == fieldName
      | Validation.NoValidation({field}) => Field(field) == fieldName
      | Validation.StringNonEmpty({field}) => Field(field) == fieldName
      | Validation.StringRegExp({field}) => Field(field) == fieldName
      | Validation.StringMin({field}) => Field(field) == fieldName
      | Validation.StringMax({field}) => Field(field) == fieldName
      | Validation.Custom({field}) => Field(field) == fieldName
      | Validation.CustomNestedSchema({field}) => Field(field) == fieldName
      | Validation.CustomNestedSchema2({field}) => Field(field) == fieldName
      | Validation.OptionNonEmpty({field}) => Field(field) == fieldName
      | Validation.ArrayNonEmpty({field}) => Field(field) == fieldName
      }
    )

  let validateOne = (
    ~field: field,
    ~values,
    ~i18n=ReSchemaI18n.default,
    schema: Validation.schema,
  ) => {
    getFieldValidators(~validators=schema, ~fieldName=field)
    ->Belt.Array.map(validator => validateField(~validator, ~values, ~i18n))
    ->Belt.Array.getBy(validation => {
      switch validation {
      | (_, Error(_)) => true
      | _ => false
      }
    })
  }

  let validateFields = (
    ~fields,
    ~values,
    ~i18n=ReSchemaI18n.default,
    schema: Validation.schema,
  ) => {
    Belt.Array.map(fields, field =>
      getFieldValidator(~validators=schema, ~fieldName=field)->Belt.Option.map(validator =>
        validateField(~validator, ~values, ~i18n)
      )
    )
  }

  let rec getIsNestedStateErrored = nestedErrors2 =>
    nestedErrors2->Array.some(form => {
      form->Array.some(((_, fieldState)) => {
        switch fieldState {
        | Error(_)
        | NestedErrors(_) => true
        | NestedErrors2(nextLevel) => getIsNestedStateErrored(nextLevel)
        | _ => false
        }
      })
    })
  let validate = (~i18n=ReSchemaI18n.default, values: Lenses.state, schema: Validation.schema) => {
    let validationList =
      schema->Belt.Array.map(validator => validateField(~validator, ~values, ~i18n))

    let errors = validationList->Belt.Array.keepMap(((field, fieldState)) =>
      switch fieldState {
      | Error(_) as e => Some((field, e))
      | NestedErrors(_) as e => Some((field, e))
      | NestedErrors2(nestedErrors2) as e =>
        getIsNestedStateErrored(nestedErrors2) ? Some((field, e)) : None
      | _ => None
      }
    )

    Belt.Array.length(errors) > 0 ? Errors(errors) : Valid
  }

  let validateAndReturn = (
    ~i18n=ReSchemaI18n.default,
    values: Lenses.state,
    schema: Validation.schema,
  ) => {
    let validationList =
      schema->Belt.Array.map(validator => validateField(~validator, ~values, ~i18n))

    validationList
  }
}
