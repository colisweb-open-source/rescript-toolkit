module type Config = {
  type field<'a>
  type state
  let set: (state, field<'a>, 'a) => state
  let get: (state, field<'a>) => 'a
  type error
}

type formState<'error> =
  | Pristine
  | Dirty
  | Errored
  | Valid
  | Submitting
  | SubmitFailed(option<'error>)
  | SubmitSucceed

type childFieldError = ReSchema.childFieldError

module Make = (Config: Config) => {
  module ReSchema = ReSchema.Make(Config)
  module Validation = ReSchema.Validation

  type field = ReSchema.field
  type formState = formState<Config.error>

  type rec fieldState =
    | Pristine
    | Valid
    | NestedErrors(array<childFieldError>)
    | NestedErrors2(array<array<(field, fieldState)>>)
    | Error(string)

  type path = {index: int, subfield: field}

  type rec action =
    | ValidateField(field)
    | ValidateForm(bool)
    | TrySubmit
    | Submit
    | SetFieldsState(array<(field, fieldState)>)
    | FieldChangeState(field, fieldState)
    | FieldChangeValue(Config.field<'a>, 'a): action
    | FieldArrayAdd(Config.field<array<'a>>, 'a): action
    | FieldArrayUpdateByIndex(Config.field<array<'a>>, 'a, int): action
    | FieldArrayRemove(Config.field<array<'a>>, int): action
    | FieldArrayRemoveBy(Config.field<array<'a>>, 'a => bool): action
    | SetFormState(formState)
    | ResetForm
    | SetValues(Config.state)
    | SetFieldValue(Config.field<'a>, 'a): action
    | RaiseSubmitFailed(option<Config.error>)

  type state = {
    formState: formState,
    values: Config.state,
    fieldsState: array<(field, fieldState)>,
  }

  type api = {
    state: state,
    getFieldState: field => fieldState,
    getFieldError: field => option<string>,
    getNestedFieldError: (field, string, int) => option<string>,
    getNestedFieldError2: (field, array<path>, ~fieldState: fieldState=?, unit) => option<string>,
    handleChange: 'a. (Config.field<'a>, 'a) => unit,
    arrayPush: 'a. (Config.field<array<'a>>, 'a) => unit,
    arrayUpdateByIndex: 'a. (~field: Config.field<array<'a>>, ~index: int, 'a) => unit,
    arrayRemoveByIndex: 'a. (Config.field<array<'a>>, int) => unit,
    arrayRemoveBy: 'a. (Config.field<array<'a>>, 'a => bool) => unit,
    submit: unit => unit,
    resetForm: unit => unit,
    setValues: Config.state => unit,
    setFieldValue: 'a. (Config.field<'a>, 'a, ~shouldValidate: bool=?, unit) => unit,
    validateField: field => unit,
    validateForm: unit => unit,
    validateFields: array<field> => array<fieldState>,
    raiseSubmitFailed: option<Config.error> => unit,
    isSubmitting: bool,
    isErrored: bool,
    isDirty: bool,
    isValid: bool,
    setFormState: formState => unit,
  }

  type onSubmitAPI = {
    send: action => unit,
    state: state,
    raiseSubmitFailed: option<Config.error> => unit,
  }

  type fieldInterface<'value> = {
    handleChange: 'value => unit,
    error: option<string>,
    state: fieldState,
    validate: unit => unit,
    value: 'value,
  }

  type validationStrategy =
    | OnChange
    | OnDemand

  let getInitialFieldsState: Validation.schema => array<(field, fieldState)> = schema => {
    schema->Belt.Array.map(validator =>
      switch validator {
      | Validation.IntMin({field}) => (ReSchema.Field(field), (Pristine: fieldState))
      | Validation.True({field}) => (Field(field), Pristine)
      | Validation.False({field}) => (Field(field), Pristine)
      | Validation.IntMax({field}) => (Field(field), Pristine)
      | Validation.FloatMin({field}) => (Field(field), Pristine)
      | Validation.FloatMax({field}) => (Field(field), Pristine)
      | Validation.Email({field}) => (Field(field), Pristine)
      | Validation.Phone({field}) => (Field(field), Pristine)
      | Validation.Password({field}) => (Field(field), Pristine)
      | Validation.OptionalPassword({field}) => (Field(field), Pristine)
      | Validation.NoValidation({field}) => (Field(field), Pristine)
      | Validation.StringNonEmpty({field}) => (Field(field), Pristine)
      | Validation.StringRegExp({field}) => (Field(field), Pristine)
      | Validation.StringMin({field}) => (Field(field), Pristine)
      | Validation.StringMax({field}) => (Field(field), Pristine)
      | Validation.Custom({field}) => (Field(field), Pristine)
      | Validation.CustomNestedSchema({field}) => (Field(field), Pristine)
      | Validation.CustomNestedSchema2({field}) => (Field(field), Pristine)
      | Validation.OptionNonEmpty({field}) => (Field(field), Pristine)
      | Validation.ArrayNonEmpty({field}) => (Field(field), Pristine)
      }
    )
  }

  let formContext: React.Context.t<api> = React.createContext(Obj.magic(None))

  let useFormContext = () => React.useContext(formContext)

  let useField = field => {
    let {handleChange, getFieldError, getFieldState, validateField, state} = useFormContext()

    {
      handleChange: handleChange(field, ...),
      error: getFieldError(Field(field)),
      state: getFieldState(Field(field)),
      validate: () => validateField(Field(field)),
      value: state.values->Config.get(field),
    }
  }

  module FormProvider = {
    let make = React.Context.provider(formContext)
  }

  module Provider = {
    @react.component
    let make = (~id=?, ~className=?, ~children, ~form) => {
      <FormProvider value={form}>
        <form
          ?id
          ?className
          onSubmit={event => {
            ReactEvent.Synthetic.preventDefault(event)
            if !form.isSubmitting {
              form.submit()
            }
          }}>
          children
          <Toolkit__Ui_VisuallyHidden>
            <input type_="submit" hidden=true />
          </Toolkit__Ui_VisuallyHidden>
        </form>
      </FormProvider>
    }
  }

  module Field = {
    @react.component
    let make = (
      ~field: Config.field<'a>,
      ~render: fieldInterface<'a> => React.element,
      ~renderOnMissingContext=React.null,
      (),
    ) => {
      let fieldInterface = useField(field)

      render(fieldInterface)
    }
  }

  let use = (
    ~initialState,
    ~schema=[]: Validation.schema,
    ~onSubmit,
    ~onSubmitFail=ignore,
    ~i18n: ReSchemaI18n.t=ReSchemaI18n.default,
    ~validationStrategy=OnChange,
    (),
  ) => {
    let (state, send) = ReactUpdate.useReducerWithMapState(
      (state, action) => {
        switch action {
        | Submit =>
          state.formState == Submitting
            ? NoUpdate
            : UpdateWithSideEffects(
                {...state, formState: Submitting},
                self => {
                  onSubmit({
                    send: self.send,
                    state: self.state,
                    raiseSubmitFailed: error => self.send(RaiseSubmitFailed(error)),
                  })
                  ->Promise.tap(x =>
                    switch x {
                    | Ok(_) => self.send(SetFormState(SubmitSucceed))
                    | Error(error) => self.send(SetFormState(SubmitFailed(Some(error))))
                    }
                  )
                  ->ignore

                  None
                },
              )
        | TrySubmit =>
          SideEffects(
            self => {
              self.send(ValidateForm(true))
              None
            },
          )
        | SetFieldsState(fieldsState) => Update({...state, fieldsState})
        | ValidateField(field) =>
          SideEffects(
            self => {
              let fieldState = ReSchema.validateOne(
                ~field,
                ~values=self.state.values,
                ~i18n,
                schema,
              )
              let newFieldState: fieldState = switch fieldState {
              | None => Valid
              | Some(fieldState) =>
                switch fieldState {
                | (_, Error(message)) => Error(message)
                | (_, NestedErrors(errors)) => NestedErrors(errors)
                | (_, NestedErrors2(errors)) => NestedErrors2(errors->Obj.magic)
                | (_, Valid) => Valid
                }
              }

              let newFieldsState =
                state.fieldsState
                ->Belt.Array.keep(((value, _)) => value != field)
                ->Belt.Array.concat([(field, newFieldState)])

              self.send(SetFieldsState(newFieldsState))
              None
            },
          )
        | ValidateForm(submit) =>
          SideEffects(
            self => {
              let recordState = ReSchema.validate(~i18n, self.state.values, schema)

              switch recordState {
              | Valid => {
                  let newFieldsState: array<(field, fieldState)> =
                    self.state.fieldsState->Belt.Array.map(((field, _)) => (
                      field,
                      (Valid: fieldState),
                    ))

                  self.send(SetFormState(Valid))
                  self.send(SetFieldsState(newFieldsState))
                  submit ? self.send(Submit) : ()
                }
              | Errors(erroredFields) =>
                let newFieldsState: array<(field, fieldState)> = erroredFields->Belt.Array.map(((
                  field,
                  err,
                )) => (
                  field,
                  switch err {
                  | Error(e) => Error(e)
                  | NestedErrors(e) => NestedErrors(e)
                  | NestedErrors2(e) => NestedErrors2(e->Obj.magic)
                  | Valid => Valid
                  },
                ))
                self.send(SetFieldsState(newFieldsState))
                submit
                  ? onSubmitFail({
                      send: self.send,
                      state: {
                        ...self.state,
                        fieldsState: newFieldsState,
                      },
                      raiseSubmitFailed: error => self.send(RaiseSubmitFailed(error)),
                    })
                  : ()
                self.send(SetFormState(Errored))
              }
              None
            },
          )
        | FieldChangeValue(field, value) =>
          UpdateWithSideEffects(
            {
              ...state,
              formState: state.formState == Errored ? Errored : Dirty,
              values: Config.set(state.values, field, value),
            },
            self => {
              switch validationStrategy {
              | OnChange => self.send(ValidateField(Field(field)))
              | OnDemand => ()
              }
              None
            },
          )
        | FieldChangeState(_, _) => NoUpdate
        | FieldArrayAdd(field, entry) =>
          Update({
            ...state,
            values: Config.set(
              state.values,
              field,
              Belt.Array.concat(Config.get(state.values, field), [entry]),
            ),
          })
        | FieldArrayRemove(field, index) =>
          Update({
            ...state,
            values: Config.set(
              state.values,
              field,
              Config.get(state.values, field)->Belt.Array.keepWithIndex((_, i) => i != index),
            ),
          })
        | FieldArrayRemoveBy(field, predicate) =>
          Update({
            ...state,
            values: Config.set(
              state.values,
              field,
              Config.get(state.values, field)->Belt.Array.keep(entry => !predicate(entry)),
            ),
          })
        | FieldArrayUpdateByIndex(field, value, index) =>
          UpdateWithSideEffects(
            {
              ...state,
              values: Config.set(
                state.values,
                field,
                Config.get(state.values, field)->Belt.Array.mapWithIndex((i, currentValue) =>
                  i == index ? value : currentValue
                ),
              ),
            },
            self => {
              switch validationStrategy {
              | OnChange => self.send(ValidateField(Field(field)))
              | OnDemand => ()
              }
              None
            },
          )
        | SetFormState(newState) => Update({...state, formState: newState})
        | ResetForm =>
          Update({
            fieldsState: getInitialFieldsState(schema),
            values: initialState,
            formState: Pristine,
          })
        | SetValues(values) => Update({...state, values})
        | SetFieldValue(field, value) =>
          Update({...state, values: Config.set(state.values, field, value)})
        | RaiseSubmitFailed(err) => Update({...state, formState: SubmitFailed(err)})
        }
      },
      () => {
        fieldsState: getInitialFieldsState(schema),
        values: initialState,
        formState: Pristine,
      },
    )

    let getFieldState = field => {
      let tmp =
        state.fieldsState
        ->Belt.List.fromArray
        ->Belt.List.getBy(((nameField, _nameFieldState)) =>
          switch nameField == field {
          | true => true
          | _ => false
          }
        )

      switch tmp {
      | Some((_nameField, nameFieldState)) => nameFieldState
      | None => Pristine
      }
    }

    let getFieldError = field =>
      (
        x =>
          switch x {
          | Error(error) => Some(error)
          | NestedErrors(_errors) => None
          | _ => None
          }
      )(getFieldState(field))

    let getNestedFieldError = (field, subfield, index) =>
      switch getFieldState(field) {
      | NestedErrors(errors) =>
        errors
        ->Array.getBy(error => error.index === index && error.name === subfield)
        ->Option.map(({error}) => error)
      | NestedErrors2(_) => None
      | Pristine
      | Valid
      | Error(_) =>
        None
      }

    let rec getNestedFieldError2 = (field, path: array<path>, ~fieldState=?, ()) => {
      switch fieldState->Option.getWithDefault(getFieldState(field)) {
      | NestedErrors2(nestedFieldStates) =>
        nestedFieldStates[(path[0]->Option.getExn).index]->Option.mapWithDefault(None, subform => {
          let subfield = subform->Array.getBy(((subfield, subfieldState)) => {
            switch (path, subfield, (path[0]->Option.getExn).subfield, subfieldState) {
            | ([_], _, _, NestedErrors2(_))
            | (_, _, _, Valid)
            | (_, _, _, Pristine) => false
            | (arr, _, _, Error(_)) if arr->Array.length >= 2 => false
            | (_, ReSchema.Field(field), ReSchema.Field(field2), _) => field === field2->Obj.magic
            }
          })

          switch subfield {
          | None => getNestedFieldError2(field, [], ~fieldState=Valid, ())
          | Some((_subfield, subfieldState)) =>
            getNestedFieldError2(field, path->Array.sliceToEnd(1), ~fieldState=subfieldState, ())
          }
        })

      | Pristine
      | Valid
      | NestedErrors(_) =>
        None
      | Error(err) => Some(err)
      }
    }

    let validateFields = (fields: array<field>) => {
      let fieldsValidated = ReSchema.validateFields(~fields, ~values=state.values, ~i18n, schema)

      let newFieldsState = Belt.Array.map(state.fieldsState, fieldStateItem => {
        let (field, _) = fieldStateItem

        Belt.Array.some(fields, fieldItem => fieldItem == field)
          ? {
              let newFieldState =
                fieldsValidated
                ->Belt.Array.keep(fieldStateValidated =>
                  Belt.Option.map(fieldStateValidated, ((item, _)) => item) == Some(field)
                )
                ->Belt.Array.get(0)

              switch newFieldState {
              | Some(fieldStateValidated) =>
                switch fieldStateValidated {
                | Some((_, newFieldStateValidated)) =>
                  switch newFieldStateValidated {
                  | Valid => [(field, (Valid: fieldState))]
                  | Error(message) => [(field, Error(message))]
                  | NestedErrors(message) => [(field, NestedErrors(message))]
                  | NestedErrors2(a) => [(field, NestedErrors2(a->Obj.magic))]
                  }

                | None => []
                }
              | None => []
              }
            }
          : [fieldStateItem]
      })->Belt.Array.reduce([], (acc, fieldState) => Belt.Array.concat(acc, fieldState))

      send(SetFieldsState(newFieldsState))

      Belt.Array.keep(newFieldsState, ((field, _)) =>
        Belt.Array.some(fields, fieldItem => fieldItem == field)
      )->Belt.Array.map(fieldState => {
        let (_, fieldStateValidation) = fieldState

        fieldStateValidation
      })
    }

    let raiseSubmitFailed = error => send(RaiseSubmitFailed(error))

    let interface: api = {
      state,
      submit: () => send(TrySubmit),
      resetForm: () => send(ResetForm),
      setValues: values => send(SetValues(values)),
      setFieldValue: (field, value, ~shouldValidate=true, ()) =>
        shouldValidate ? send(FieldChangeValue(field, value)) : send(SetFieldValue(field, value)),
      getFieldState,
      getFieldError,
      getNestedFieldError,
      getNestedFieldError2,
      handleChange: (field, value) => send(FieldChangeValue(field, value)),
      arrayPush: (field, value) => send(FieldArrayAdd(field, value)),
      arrayUpdateByIndex: (~field, ~index, value) =>
        send(FieldArrayUpdateByIndex(field, value, index)),
      arrayRemoveBy: (field, predicate) => send(FieldArrayRemoveBy(field, predicate)),
      arrayRemoveByIndex: (field, index) => send(FieldArrayRemove(field, index)),
      validateField: field => send(ValidateField(field)),
      validateForm: () => send(ValidateForm(false)),
      validateFields,
      raiseSubmitFailed,
      isSubmitting: state.formState == Submitting,
      isErrored: state.formState == Errored,
      isDirty: state.formState == Dirty,
      isValid: state.formState == Valid,
      setFormState: formState => send(SetFormState(formState)),
    }

    interface
  }
}
