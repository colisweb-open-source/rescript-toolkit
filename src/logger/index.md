# Logger

Display log only at development and trigger Sentry on error on production.

## Usage

```reason
let obj = {
  "test": 1,
  "z": "ee"
}

Toolkit.BrowserLogger.debug("test")

Toolkit.BrowserLogger.error2("Error", obj)
```
