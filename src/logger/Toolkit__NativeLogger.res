open SentryReactNative

@val external isDev: bool = "__DEV__"

let debug = str => {
  if isDev {
    Js.Console.log2("DEBUG", str)
  }
}

let debug2 = (str, str2) => {
  if isDev {
    Js.Console.log3("DEBUG", str, str2)
  }
}

let debugMany = (arr: array<string>) => {
  if isDev {
    Js.Console.logMany(["DEBUG"]->Array.concat(arr))
  }
}

let warning = str => {
  if isDev {
    Js.Console.log2("WARN", str)
  }
}

let warning2 = (str, str2) => {
  if isDev {
    Js.Console.log3("WARN", str, str2)
  }
}

let warningMany = (arr: array<string>) => {
  if isDev {
    Js.Console.logMany(["WARN"]->Array.concat(arr))
  }
}

let error = str => {
  if isDev {
    Js.Console.log2("ERROR", str)
  } else {
    open Sentry
    withScope(_scope => {
      captureMessage(Obj.magic(str))
    })
  }
}

let error2 = (str, str2) => {
  if isDev {
    Js.Console.log3("ERROR", str, str2)
  } else {
    open Sentry
    withScope(scope => {
      scope->Scope.setExtra("data", str2)
      captureMessage(Obj.magic(str))
    })
  }
}

let errorMany = (arr: array<string>) => {
  let message = arr[0]
  let data = arr->Array.length > 1 ? arr->Array.sliceToEnd(1) : []
  if isDev {
    Js.Console.logMany(["ERROR"]->Array.concat(arr))
  } else {
    open Sentry
    withScope(scope => {
      scope->Scope.setExtra("data", data)
      captureMessage(Obj.magic(message))
    })
  }
}
