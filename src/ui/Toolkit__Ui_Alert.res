type status = [#success | #error | #warning | #info]
type size = [#xs | #sm | #md]
@react.component
let make = (
  ~title,
  ~description=?,
  ~status,
  ~className=?,
  ~icon: option<module(ReactIcons.Icon)>=?,
  ~size: size=#md,
  ~borderless=false,
) =>
  <div
    className={cx([
      "flex  border rounded-lg",
      switch status {
      | #success => "bg-success-50 border-success-500"
      | #error => "bg-danger-50 border-danger-500"
      | #warning => "bg-warning-50 border-warning-500"
      | #info => "bg-info-50 border-info-500"
      },
      className->Option.getWithDefault(""),
      switch size {
      | #md => "p-4"
      | #sm => "p-2"
      | #xs => "p-1"
      },
      borderless ? "!border-none" : "",
    ])}>
    {switch (status, icon) {
    | (#success, None) =>
      <ReactIcons.MdCheckCircle size=28 className="text-success-600 flex-shrink-0" />
    | (#success, Some(module(Icon))) => <Icon size=28 className="text-success-600 flex-shrink-0" />
    | (#error, None) => <ReactIcons.MdWarning size=28 className="text-danger-600 flex-shrink-0" />
    | (#error, Some(module(Icon))) => <Icon size=28 className="text-danger-600 flex-shrink-0" />
    | (#warning, None) =>
      <ReactIcons.FaExclamationTriangle size=28 className="text-warning-600 flex-shrink-0" />
    | (#warning, Some(module(Icon))) => <Icon size=28 className="text-warning-600 flex-shrink-0" />
    | (#info, None) => <ReactIcons.FaInfoCircle size=28 className="text-info-600 flex-shrink-0" />
    | (#info, Some(module(Icon))) => <Icon size=28 className="text-info-600 flex-shrink-0" />
    }}
    <div
      className={cx([
        switch size {
        | #md => "mx-3"
        | #sm => "mx-2"
        | #xs => "mx-1"
        },
      ])}>
      <div
        className={cx([
          description->Option.isSome ? "font-semibold" : "",
          {
            switch status {
            | #success => "text-success-600"
            | #error => "text-danger-600"
            | #warning => "text-warning-700"
            | #info => "text-info-600"
            }
          },
        ])}>
        title
      </div>
      {description->Option.getWithDefault(React.null)}
    </div>
  </div>
