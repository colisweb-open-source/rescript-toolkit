open Radix
type item = {
  label: React.element,
  rightSlot?: React.element,
}

type itemContent = {
  ...DropdownMenu.Item.options,
  ...item,
}
type subTrigger = {...DropdownMenu.SubTrigger.options, ...item}

type content =
  | Item(itemContent)
  | Separator
  | Label(React.element)
  | SubItems(subTrigger, array<itemContent>)

%%private(let renderRightSlot = rightSlot => <div className="RightSlot"> rightSlot </div>)

@react.component
let make = (
  ~trigger,
  ~triggerOptions: option<DropdownMenu.Trigger.options>=?,
  ~rootOptions: option<DropdownMenu.Root.options>=?,
  ~contentOptions: option<DropdownMenu.Content.options>=?,
  ~content: array<content>,
) => {
  <DropdownMenu.Root {...(rootOptions->Option.getWithDefault({})->Obj.magic)}>
    <DropdownMenu.Trigger {...(triggerOptions->Option.getWithDefault({})->Obj.magic)}>
      trigger
    </DropdownMenu.Trigger>
    <DropdownMenu.Portal>
      <DropdownMenu.Content
        {...(contentOptions->Option.getWithDefault({})->Obj.magic)}
        className="DropdownMenuContent"
        sideOffset={contentOptions
        ->Option.flatMap(v => v.sideOffset)
        ->Option.getWithDefault(5)}>
        {content
        ->Array.mapWithIndex((i, content) => {
          switch content {
          | Label(element) =>
            <DropdownMenu.Label
              key={i->Int.toString ++ "dropdown-menu-item"} className="DropdownMenuLabel">
              element
            </DropdownMenu.Label>
          | Separator =>
            <DropdownMenu.Separator
              key={i->Int.toString ++ "dropdown-menu-item"} className="DropdownMenuSeparator"
            />
          | Item(item) =>
            <DropdownMenu.Item
              key={i->Int.toString ++ "dropdown-menu-item"}
              className="DropdownMenuItem"
              disabled=?{item.disabled}
              onSelect=?{item.onSelect}>
              {item.label}
              {item.rightSlot->Option.mapWithDefault(React.null, renderRightSlot)}
            </DropdownMenu.Item>

          | SubItems(item, subContent) =>
            <DropdownMenu.Sub key={i->Int.toString ++ "dropdown-menu-item"}>
              <DropdownMenu.SubTrigger className="DropdownMenuSubTrigger" disabled=?{item.disabled}>
                {item.label}
                {item.rightSlot->Option.mapWithDefault(React.null, renderRightSlot)}
              </DropdownMenu.SubTrigger>
              <DropdownMenu.Portal>
                <DropdownMenu.SubContent
                  className="DropdownMenuSubContent" sideOffset={2} alignOffset={-5}>
                  {subContent
                  ->Array.mapWithIndex((j, item) => {
                    <DropdownMenu.Item
                      key={i->Int.toString ++ j->Int.toString ++ "dropdown-menu-item"}
                      disabled=?{item.disabled}
                      onSelect=?{item.onSelect}
                      className="DropdownMenuItem">
                      {item.label}
                      {item.rightSlot->Option.mapWithDefault(React.null, renderRightSlot)}
                    </DropdownMenu.Item>
                  })
                  ->React.array}
                </DropdownMenu.SubContent>
              </DropdownMenu.Portal>
            </DropdownMenu.Sub>
          }
        })
        ->React.array}
        <DropdownMenu.Arrow className="DropdownMenuArrow" />
      </DropdownMenu.Content>
    </DropdownMenu.Portal>
  </DropdownMenu.Root>
}
