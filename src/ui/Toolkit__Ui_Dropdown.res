type position = [
  | #bottom
  | #right
  | #top
  | #left
]

type dropdownContext = {hide: unit => unit}

let dropdownContext = React.createContext({
  hide: () => (),
})

module Provider = {
  let make = React.Context.provider(dropdownContext)
}

@react.component
let make = (
  ~label: React.element,
  ~children,
  ~dropdownClassName="",
  ~buttonClassName="",
  ~containerClassName="",
  ~defaultIsOpen=false,
  ~buttonColor: Toolkit__Ui_Button.color=#white,
  ~buttonSize: Toolkit__Ui_Button.size=#md,
  ~buttonVariant: Toolkit__Ui_Button.variant=#default,
  ~buttonLeftIcon=?,
  ~position=#bottom,
  ~disabled: option<bool>=?,
  ~onClose: option<unit => unit>=?,
) => {
  let dropDownRef = React.useRef(Js.Nullable.null)
  let buttonRef = React.useRef(Js.Nullable.null)
  let (position, setPosition) = React.useState(() => position)
  let {isOpen, hide, toggle} = Toolkit__Hooks.useDisclosure(~defaultIsOpen, ())

  Toolkit__Hooks.useOnClickOutside(buttonRef, _ => {
    hide()
    onClose->Option.forEach(fn => fn())
  })

  let (adjustmentStyle, setAdjustmentStyle) = React.useState(() => None)

  React.useEffect(() => {
    if isOpen {
      dropDownRef.current
      ->Js.Nullable.toOption
      ->Option.forEach(dom => {
        let {left, right} = dom->Browser.DomElement.getBoundingClientRect

        switch position {
        | #left if left < 0. => setPosition(_ => #right)
        | #right if right < 0. => setPosition(_ => #left)
        | _ =>
          setAdjustmentStyle(
            _ => Some(
              ReactDOM.Style.make(
                ~marginLeft=left < 0. ? `${-.left->Js.Float.toString}px` : "",
                ~marginRight=right < 0. ? `${-.right->Js.Float.toString}px` : "",
                ~opacity="1",
                (),
              ),
            ),
          )
        }
      })
    } else {
      setAdjustmentStyle(_ => None)
    }
    None
  }, (isOpen, dropDownRef))

  <div className={cx(["relative", containerClassName])}>
    <Toolkit__Ui_Button
      variant=buttonVariant
      size=buttonSize
      ?disabled
      type_="button"
      leftIcon=?{buttonLeftIcon}
      color=buttonColor
      buttonRef={ReactDOM.Ref.domRef(buttonRef)}
      onClick={event => {
        switch dropDownRef.current->Js.Nullable.toOption {
        | None => toggle()
        | Some(dropDownRef) => {
            let clickedElement = event->ReactEvent.Mouse.target->Browser.DomElement.toDomElement

            if !(dropDownRef->Browser.DomElement.Node.contains(clickedElement)) {
              toggle()
            }
          }
        }
      }}
      className={cx([buttonClassName, "dropdown-button"])}>
      label
      {isOpen
        ? <div
            ref={ReactDOM.Ref.domRef(dropDownRef)}
            className={cx([
              "dropdown",
              "absolute  z-20 bg-white p-2 transform shadow rounded text-base font-normal text-neutral-700 opacity-0",
              switch position {
              | #bottom => "left-1/2 top-full  -translate-x-1/2 mt-2"
              | #top => "left-1/2 bottom-full -translate-x-1/2 mb-2"
              | #left => "right-full top-1/2 -translate-y-1/2 ml-2"
              | #right => "left-full top-1/2 -translate-y-1/2 ml-2"
              },
              dropdownClassName,
            ])}
            style={adjustmentStyle->Option.getWithDefault(ReactDOM.Style.make())}>
            <Provider value={{hide: hide}}> children </Provider>
          </div>
        : React.null}
    </Toolkit__Ui_Button>
  </div>
}
