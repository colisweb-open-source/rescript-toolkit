open ReactIntl

type rec state = {
  period: period,
  selectedDay: Js.Date.t,
}
and period = {
  start: Js.Date.t,
  end_: Js.Date.t,
}

type action =
  | PreviousPeriod
  | NextPeriod
  | UpdateSelectedDay(Js.Date.t)

let updateQueryParams = (state: state) => {
  RescriptReactRouter.replace(
    Qs.stringifyWithParams(
      {
        "start": Some(state.period.start),
        "selectedDay": state.selectedDay,
      }->Obj.magic,
      Qs.makeParams(
        ~addQueryPrefix=true,
        ~serializeDate=d => d->DateFns.formatWithPattern("yyyy-MM-dd"),
        (),
      ),
    ),
  )
}

/*
 * The key is a formatted date
 * ex:
 * {
 *   "2023-03-03": 2
 * }
 */
type dayCounter = Js.Dict.t<int>

@react.component
let make = (
  ~selectedDay: option<Js.Date.t>=?,
  ~className="",
  ~containerClassName="",
  ~counters: option<dayCounter>=?,
  ~periodLength=?, // desktop periodLength ! does'nt work on mobile (always 3 on mobile, default is 7 on desktop )
  ~children,
  ~onChange: option<state => unit>=?,
  ~disabledBefore: option<Js.Date.t>=?,
  ~showMonths=true,
  ~selectedDateClassName="",
  ~dateClassName="",
  ~periodButtonClassName="",
) => {
  let {isSm} = Toolkit__Hooks.useMediaQuery()

  let periodLength = periodLength->Option.getWithDefault(isSm ? 7 : 3)

  let ({period, selectedDay}, dispatch) = ReactUpdate.useReducerWithMapState(
    (state, action) =>
      switch action {
      | PreviousPeriod =>
        let previousPeriodStart = state.period.start->DateFns.subDays(periodLength)
        let selectedDay =
          disabledBefore->Option.mapWithDefault(previousPeriodStart, disabledBefore =>
            previousPeriodStart->DateFns.isBefore(disabledBefore)
              ? disabledBefore
              : previousPeriodStart
          )
        UpdateWithSideEffects(
          {
            period: {
              start: state.period.start->DateFns.subDays(periodLength)->DateFns.startOfDay,
              end_: state.period.start->DateFns.subDays(1),
            },
            selectedDay,
          },
          ({state}) => {
            updateQueryParams(state)
            onChange->Option.forEach(fn => fn(state))

            None
          },
        )
      | NextPeriod =>
        UpdateWithSideEffects(
          {
            period: {
              start: state.period.end_->DateFns.addDays(1),
              end_: state.period.end_->DateFns.addDays(periodLength),
            },
            selectedDay: state.period.end_->DateFns.addDays(1),
          },
          ({state}) => {
            updateQueryParams(state)
            onChange->Option.forEach(fn => fn(state))

            None
          },
        )
      | UpdateSelectedDay(date) =>
        UpdateWithSideEffects(
          {...state, selectedDay: date},
          ({state}) => {
            updateQueryParams(state)
            onChange->Option.forEach(fn => fn(state))

            None
          },
        )
      },
    () => {
      let today = Js.Date.make()
      // selectedDay with default
      let selectedDay = selectedDay->Option.getWithDefault(today)
      // we check that the selectedDay is not in disabled range
      let selectedDay =
        disabledBefore->Option.mapWithDefault(selectedDay, disabledBefore =>
          selectedDay->DateFns.isBefore(disabledBefore) ? disabledBefore : selectedDay
        )

      // we build the default displayed period around the selectedDay
      let defaultPeriod: period = {
        let periodStart = periodLength === 7 ? DateFns.startOfWeek(selectedDay) : selectedDay

        let periodEnd = periodStart->DateFns.addDays(periodLength - 1)

        {
          start: periodStart,
          end_: periodEnd,
        }
      }

      {
        period: defaultPeriod,
        selectedDay,
      }
    },
  )
  let intl = useIntl()

  let days = DateFns.eachDayOfInterval({
    start: period.start,
    end_: period.end_,
  })

  <div className={containerClassName}>
    <div className={cx(["border-neutral-300 flex justify-center items-center py-3", className])}>
      <Toolkit__Ui_IconButton
        size=#xs
        variant=#outline
        color=#neutral
        onClick={_ => dispatch(PreviousPeriod)}
        className=periodButtonClassName
        ariaLabel="previous week"
        disabled=?{disabledBefore->Option.map(disabledBefore => {
          let isPreviousPeriodOutOfBounds =
            period.start->DateFns.subDays(1)->DateFns.isBefore(disabledBefore)
          isPreviousPeriodOutOfBounds
        })}
        icon={<ReactIcons.MdKeyboardArrowLeft size=30 />}
      />
      <div className="flex items-center gap-1 mx-1 md:mx-2">
        {days
        ->Array.mapWithIndex((i, day) => {
          let formattedDay =
            intl->Intl.formatDateWithOptions(day, dateTimeFormatOptions(~weekday=#long, ()))

          let isSelected = DateFns.isSameDay(selectedDay, day)

          let isDisabled =
            disabledBefore->Option.mapWithDefault(false, disabledBefore =>
              day->DateFns.isBefore(disabledBefore)
            )
          <React.Fragment key={i->Int.toString ++ "-day"}>
            {(i == 0 || day->DateFns.isFirstDayOfMonth) && showMonths
              ? <p className="text-xs text-neutral-700 uppercase">
                  <FormattedDate value=day month=#short />
                </p>
              : React.null}
            <div
              onClick={_ => isDisabled ? () : dispatch(UpdateSelectedDay(day))}
              className={cx([
                "flex flex-col items-stretch w-14 sm:w-16 font-display",
                isDisabled ? "opacity-50 cursor-not-allowed" : "",
                isSelected ? selectedDateClassName : "",
                dateClassName,
              ])}>
              <p
                className={cx([
                  "flex flex-col items-center justify-center uppercase text-xs w-full rounded-sm leading-tight py-1",
                  isSelected ? "text-white bg-primary-700 " : "text-primary-700 bg-primary-50 ",
                ])}>
                <span>
                  {formattedDay
                  ->Js.String2.slice(~from=0, ~to_=4)
                  ->Js.String2.concat(formattedDay->Js.String2.length > 4 ? "." : "")
                  ->React.string}
                </span>
                <span className="text-sm font-semibold">
                  <FormattedDate value=day day=#"2-digit" />
                </span>
              </p>
              {counters
              ->Option.flatMap(counters =>
                counters->Js.Dict.get(day->DateFns.formatWithPattern("yyyy-MM-dd"))
              )
              ->Option.mapWithDefault(React.null, value => {
                <p
                  className="text-xs text-info-500 bg-info-50 text-center mt-1 font-semibold rounded-sm">
                  {value->React.int}
                </p>
              })}
            </div>
          </React.Fragment>
        })
        ->React.array}
      </div>
      <Toolkit__Ui_IconButton
        size=#xs
        variant=#outline
        color=#neutral
        className=periodButtonClassName
        onClick={_ => dispatch(NextPeriod)}
        ariaLabel="next week"
        icon={<ReactIcons.MdKeyboardArrowRight size=30 />}
      />
    </div>
    {children({period, selectedDay})}
  </div>
}
