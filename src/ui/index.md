# UI

Set of components using Tailwind

## Usage

Import the CSS style in your app :

```js
// App.js
import '@colisweb/rescript-toolkit/src/ui/styles.css
```

```rescript
open Toolkit.Ui;

[@react.component]
let make = () => {
  <div>
    <Button>"Test"->React.string</Button>
  </div>
}
```
