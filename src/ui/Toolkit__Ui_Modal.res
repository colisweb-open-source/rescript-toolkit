type size = [#xs | #sm | #md | #lg | #custom(int)]
type color = [#primary | #success | #danger | #neutral | #dynamic]
type style = [#default | #colored(color)]

let modalStyle = (~type_: style) =>
  switch type_ {
  | #default => "rounded"
  | #colored(color) =>
    switch color {
    | #primary => "rounded-lg"
    | #success => "rounded-lg border-2 border-success-500"
    | #danger => "rounded-lg border-4 border-danger-700"
    | #neutral => "rounded-lg"
    | #dynamic => ""
    }
  }

let headerStyle = (~type_: style) =>
  switch type_ {
  | #default => ""
  | #colored(color) =>
    switch color {
    | #primary => "bg-primary-700 rounded-t"
    | #success => "bg-success-500 rounded-t"
    | #danger => "bg-danger-700"
    | #neutral => "bg-neutral-700 rounded-t-lg"
    | #dynamic => ""
    }
  }

let titleStyle = (~type_) =>
  switch type_ {
  | #default => "w-full border-b border-info-500"
  | #colored(#dynamic) => "w-full border-b border-primaryText"
  | #colored(_color) => "text-white"
  }

let closeIconStyle = (~type_) =>
  switch type_ {
  | #default => "hover:bg-gray-200"
  | #colored(#dynamic) => "hover:bg-gray-200"
  | #colored(_) => "hover:bg-white hover:text-black text-white"
  }

@module("react")
external fragment: React.element = "Fragment"

@react.component
let make = (
  ~isVisible,
  ~title: option<React.element>=?,
  ~body: React.element,
  ~hide,
  ~size=#md,
  ~type_=#default,
  ~footer=?,
  ~ariaLabel="",
  ~icon: option<module(ReactIcons.Icon)>=?,
) =>
  <HeadlessUi.Dialog \"open"=isVisible onClose=hide className="relative z-50">
    <HeadlessUi.Dialog.Backdrop
      transition=true
      className="fixed inset-0 bg-black/30 duration-200 ease-out data-[closed]:opacity-0"
    />
    <div className="fixed inset-0 overflow-y-auto flex min-h-full items-start justify-center p-4">
      <HeadlessUi.Dialog.Panel
        transition=true
        className={cx([
          "bg-white pb-5 shadow-lg w-full mt-[10vh] duration-300 ease-out data-[closed]:scale-95 data-[closed]:opacity-0",
          modalStyle(~type_),
        ])}
        style={ReactDOMStyle.make(
          ~maxWidth={
            let value = switch size {
            | #xs => 480
            | #sm => 600
            | #md => 768
            | #lg => 900
            | #custom(value) => value
            }
            `${value->Int.toString}px`
          },
          (),
        )}>
        <header
          className={cx([
            "flex items-center justify-between mb-4 pl-5 pr-3 pt-2 pb-1",
            headerStyle(~type_),
          ])}>
          {title->Option.mapWithDefault(React.null, title =>
            <HeadlessUi.Dialog.Title
              className={cx([
                "text-2xl pb-1 inline-flex items-center gap-2 font-display",
                titleStyle(~type_),
              ])}>
              {icon->Option.mapWithDefault(React.null, icon => {
                let module(Icon) = icon

                <Icon />
              })}
              title
            </HeadlessUi.Dialog.Title>
          )}
          <button
            onClick={_ => hide()}
            className={cx([
              title->Option.isSome ? "ml-4" : "ml-auto",
              "p-1 rounded-full modal-close-button",
              closeIconStyle(~type_),
            ])}>
            <ReactIcons.MdClose size=28 />
          </button>
        </header>
        <div className="px-1 sm:px-2 md:px-5"> body </div>
        {footer->Option.mapWithDefault(React.null, footer =>
          <div className="flex flex-row justify-end mt-6 pr-4"> footer </div>
        )}
      </HeadlessUi.Dialog.Panel>
    </div>
  </HeadlessUi.Dialog>
