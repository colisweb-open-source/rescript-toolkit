type selectOption = {
  label: string,
  value: string,
  disabled?: bool,
}

type options = array<selectOption>

@react.component
let make = (
  ~options: options,
  ~onChange: string => unit,
  ~onBlur=?,
  ~placeholder=?,
  ~defaultValue=?,
  ~isDisabled=?,
  ~isInvalid=?,
  ~className="",
  ~containerClassName="",
  ~id=?,
  ~value=?,
  ~name=?,
  ~autoFocus=?,
) =>
  <div className={cx(["relative", containerClassName])}>
    <select
      ?id
      ?name
      ?autoFocus
      disabled={isDisabled->Option.getWithDefault(false)}
      ariaDisabled={isDisabled->Option.getWithDefault(false)}
      ?onBlur
      className={cx([
        "border rounded transition-all duration-150 ease-in-out py-2 pl-4 pr-8 appearance-none w-full bg-white text-gray-800 leading-tight focus:outline-none focus:shadow-none focus:border-info-500 disabled:bg-gray-200 disabled:text-gray-700 focus:z30 relative",
        switch isInvalid {
        | Some(true) => "border-danger-500 shadow-danger-500"
        | _ => "border-gray-300"
        },
        className,
      ])}
      ?defaultValue
      ?value
      onChange={event => {
        let value = ReactEvent.Form.target(event)["value"]

        onChange(value)
      }}>
      {placeholder->Option.mapWithDefault(React.null, placeholder =>
        <option value=""> {placeholder->React.string} </option>
      )}
      {options
      ->Array.mapWithIndex((i, option) =>
        <option
          key={"select_option_" ++ i->Int.toString}
          value={option.value}
          disabled=?{option.disabled}>
          {option.label->React.string}
        </option>
      )
      ->React.array}
    </select>
    <div
      className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-800">
      <ReactIcons.FaAngleDown />
    </div>
  </div>
