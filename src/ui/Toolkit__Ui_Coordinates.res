open ReactIntl

@react.component
let make = (~coordinates: Toolkit__Decoders.Coordinates.t, ~copyWrapperClassName="") => {
  let intl = useIntl()

  <Toolkit__Ui_CopyWrapper
    intl
    className={cx(["inline-flex items-center", copyWrapperClassName])}
    textToCopy={`${coordinates.latitude->Float.toString}, ${coordinates.longitude->Float.toString}`}>
    <div className="inline-flex flex-row items-center gap-2 text-info-600">
      <ReactIcons.MdMyLocation size=15 />
      <p className="e2e-coordinates-gps">
        <FormattedMessage
          defaultMessage={"<lat></lat>, <lng></lng>"}
          values={{
            "lat": _ =>
              <FormattedNumberParts value={coordinates.latitude} maximumFractionDigits={10}>
                {(~formattedNumberParts) => {
                  switch formattedNumberParts {
                  | [{value}] => value->React.string
                  | [{value}, _] => value->React.string
                  | [val1, _, val2] =>
                    <React.Fragment>
                      {val1.value->React.string}
                      {"."->React.string}
                      {val2.value->React.string}
                    </React.Fragment>
                  | _ => React.null
                  }
                }}
              </FormattedNumberParts>,
            "lng": _ =>
              <FormattedNumberParts value={coordinates.longitude} maximumFractionDigits={10}>
                {(~formattedNumberParts) => {
                  switch formattedNumberParts {
                  | [{value}] => value->React.string
                  | [{value}, _] => value->React.string
                  | [val1, _, val2] =>
                    <React.Fragment>
                      {val1.value->React.string}
                      {"."->React.string}
                      {val2.value->React.string}
                    </React.Fragment>
                  | _ => React.null
                  }
                }}
              </FormattedNumberParts>,
          }}
        />
      </p>
    </div>
  </Toolkit__Ui_CopyWrapper>
}
