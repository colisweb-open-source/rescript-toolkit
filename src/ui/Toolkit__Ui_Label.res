@react.component
let make = (~htmlFor, ~className="", ~optionalMessage=?, ~children) =>
  <label htmlFor className={cx(["block text-neutral-700 font-semibold leading-5 mb-1", className])}>
    children
    {optionalMessage->Option.mapWithDefault(React.null, message =>
      <span className="ml-2 text-neutral-500 italic text-sm"> message </span>
    )}
  </label>
