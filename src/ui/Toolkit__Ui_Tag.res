type color = [
  | #black
  | #white
  | #gray
  | #primary
  | #info
  | #danger
  | #warning
  | #success
  | #neutral
]

type size = [#sm | #md | #lg | #xs]
type variant = [#plain | #outline]

@react.component
let make = (
  ~color=#gray,
  ~size=#md,
  ~className="",
  ~variant=#plain,
  ~tooltipLabel: option<React.element>=?,
  ~tooltipClassName=?,
  ~children,
) => {
  let disclosure = Toolkit__Hooks.useDisclosure()
  let sizeStyle = x =>
    switch x {
    | #xs => "text-xs leading-4 px-1"
    | #sm => "text-xs leading-6 px-2"
    | #md => "text-sm leading-8 px-3"
    | #lg => "text-base leading-10 px-4"
    }

  let colorStyle = c =>
    switch (c, variant) {
    | (c, #plain) =>
      switch c {
      | #black => "bg-gray-900 text-white"
      | #white => "bg-white text-gray-900"
      | #gray => "bg-gray-200 text-gray-800"
      | #primary => "bg-primary-500 text-white"
      | #info => "bg-info-100 text-info-700"
      | #danger => "bg-danger-600 text-white"
      | #warning => "bg-warning-50 text-warning-700"
      | #success => "bg-success-50 text-success-700 bg-opacity-50"
      | #neutral => "bg-gray-100 text-neutral-600"
      }
    | (c, #outline) =>
      switch c {
      | #black => "border border-gray-900 text-gray-900"
      | #white => "border border-white text-white"
      | #gray => "border border-gray-400 text-gray-700"
      | #primary => "border border-primary-500 text-primary-500"
      | #info => "border border-info-600 text-info-600 bg-info-50"
      | #danger => "border border-danger-600 text-danger-600 bg-danger-50"
      | #warning => "border bg-warning-50 text-warning-700 border-warning-700"
      | #success => "border bg-success-50 text-success-700 border-success-700"
      | #neutral => "border text-neutral-600 border-neutral-600"
      }
    }

  <Toolkit__Ui_Tooltip.Controlled
    open_={disclosure.isOpen}
    onOpenChange={_ => tooltipLabel->Option.isSome ? disclosure.toggle() : ()}
    tooltipContent={tooltipLabel->Option.getWithDefault(React.null)}
    contentClassName=?tooltipClassName
    triggerContent={<div
      className={cx([
        className,
        "inline-flex self-center rounded font-semibold max-w-xs",
        colorStyle(color),
        sizeStyle(size),
      ])}>
      <span className="truncate"> children </span>
    </div>}
  />
}
