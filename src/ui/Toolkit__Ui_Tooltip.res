open Radix

type color =
  | White
  | Black

@react.component
let make = (
  ~triggerContent,
  ~tooltipContent,
  ~side: Tooltip.side=Top,
  ~sideOffset=5,
  ~contentClassName="",
  ~color=White,
  ~defaultOpen=?,
  ~triggerAsChild=false,
) => {
  <Tooltip.Provider>
    <Tooltip.Root ?defaultOpen delayDuration={0}>
      <Tooltip.Trigger asChild=triggerAsChild type_="button"> {triggerContent} </Tooltip.Trigger>
      <Tooltip.Portal>
        <Tooltip.Content
          side
          sideOffset
          className={cx([
            "TooltipContent",
            `TooltipContent--${(color :> string)}`,
            contentClassName,
          ])}>
          {tooltipContent}
          <Tooltip.Arrow className="TooltipArrow" />
        </Tooltip.Content>
      </Tooltip.Portal>
    </Tooltip.Root>
  </Tooltip.Provider>
}

module Controlled = {
  @react.component
  let make = (
    ~triggerContent,
    ~tooltipContent,
    ~side: Tooltip.side=Top,
    ~sideOffset=5,
    ~contentClassName="",
    ~color=White,
    ~open_,
    ~onOpenChange,
    ~defaultOpen=?,
  ) => {
    <Tooltip.Provider>
      <Tooltip.Root ?defaultOpen delayDuration={0} open_ onOpenChange>
        <Tooltip.Trigger type_={"button"}> {triggerContent} </Tooltip.Trigger>
        <Tooltip.Portal>
          <Tooltip.Content
            side
            sideOffset
            className={cx([
              "TooltipContent",
              `TooltipContent--${(color :> string)}`,
              contentClassName,
            ])}>
            {tooltipContent}
            <Tooltip.Arrow className="TooltipArrow" />
          </Tooltip.Content>
        </Tooltip.Portal>
      </Tooltip.Root>
    </Tooltip.Provider>
  }
}
