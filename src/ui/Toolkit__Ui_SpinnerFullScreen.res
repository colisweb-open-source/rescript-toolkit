@react.component
let make = (~color=?, ~size=?) =>
  <div className="fixed left-0 top-0 w-full h-full flex flex-row justify-center items-center">
    <Toolkit__Ui_Spinner ?color ?size />
  </div>
