open ReactIntl

@@intl.messages
let searchError = {defaultMessage: "Une erreur est survenue lors de la recherche"}

type rec suggestions<'value> = HeadlessUi__Combobox.suggestions<'value>
and suggestion<'value> = HeadlessUi__Combobox.suggestion<'value>

type renderProps<'value, 'error> = {
  setSearch: (((string, bool)) => (string, bool)) => unit,
  setSuggestions: (
    option<result<HeadlessUi.Combobox.suggestions<'value>, 'error>> => option<
      result<HeadlessUi.Combobox.suggestions<'value>, 'error>,
    >
  ) => unit,
}

@react.component
let make = (
  ~onSelect: option<suggestion<'value>> => unit,
  ~isInvalid=false,
  ~value: option<suggestion<'value>>,
  ~onSearch,
  ~emptySearchMessage=?,
  ~containerClassName="",
  ~emptySearchClassName="",
  ~errorSearchClassName="",
  ~inputClassName="",
  ~optionsContainerClassName="",
  ~inputId=?,
  ~optionClassName="",
  ~placeholder=?,
  ~autoFocus=?,
  ~minSearchLength=3,
  ~additionnalOption=?,
  ~children: option<renderProps<'value, 'error> => React.element>=?,
) => {
  let (search, setSearch) = React.useState(() =>
    value->Option.mapWithDefault(("", false), v => {
      (v.label, true)
    })
  )
  let (search, isDefaultValue) = search
  let (suggestions, setSuggestions) = React.useState((): option<
    result<HeadlessUi.Combobox.suggestions<'value>, 'error>,
  > => None)
  let (searching, setSearching) = React.useState(() => false)
  let intl = useIntl()

  let onInputChange = event => {
    let query = ReactEvent.Form.target(event)["value"]

    setSearch(_ => (query, false))
  }

  let (_, _) = Toolkit__Hooks.useDebounce2(() => {
    if !isDefaultValue {
      switch search {
      | "" => ()
      | search if search->String.length < minSearchLength =>
        setSuggestions(_ => Some(Error("QUERY_TOO_SMALL")))
      | search =>
        setSearching(_ => true)

        onSearch(search)
        ->Promise.tapOk(suggestions => {
          setSearching(_ => false)
          setSuggestions(_ => Some(Ok(suggestions)))
        })
        ->Promise.tapError(_ => {
          setSuggestions(_ => Some(Error(intl->Intl.formatMessage(searchError))))
          setSearching(_ => false)
        })
        ->ignore
      }
    }
  }, 400, (search, isDefaultValue))

  <div className={cx(["relative", containerClassName])}>
    <HeadlessUi.Combobox
      onChange={selectedValue => {
        onSelect(selectedValue->Js.Nullable.toOption)
      }}
      value={value->Js.Nullable.fromOption}
      nullable={true}>
      <div className="relative z-0 w-full cursor-default rounded bg-white text-left">
        <HeadlessUi.Combobox.Input
          ?placeholder
          ?autoFocus
          id=?inputId
          onChange=onInputChange
          autocomplete="off"
          className={cx([
            "w-full border py-2 rounded pl-3 pr-10 text-base leading-5 text-gray-900",
            isInvalid ? "border-danger-500" : "border-gray-300",
            inputClassName,
          ])}
          displayValue={selectedValue => {
            selectedValue->Js.Nullable.toOption->Option.mapWithDefault("", ({label}) => label)
          }}
        />
        {searching
          ? <Toolkit__Ui_Spinner
              size=#sm color=#primary className="absolute right-[10px] top-[10px] z-30"
            />
          : React.null}
      </div>
      {switch suggestions {
      | None => React.null
      | Some(suggestions) =>
        <HeadlessUi.Combobox.Options
          transition=true
          className={cx([
            "z-10 absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm",
            "transition duration-200 ease-out data-[closed]:scale-95 data-[closed]:opacity-0",
            optionsContainerClassName,
          ])}>
          {switch suggestions {
          | Error("QUERY_TOO_SMALL") =>
            <p className="p-2 text-sm">
              <FormattedMessage
                defaultMessage="Tapez au moins {minSearchLength, plural, one {1 caractère} other {# caractères}}"
                values={"minSearchLength": minSearchLength}
              />
            </p>
          | Error(err) =>
            <p className={cx(["p-2 text-danger-500", errorSearchClassName])}>
              {err->React.string}
            </p>
          | Ok([]) =>
            <div className="p-2">
              <p className={cx(["text-sm", emptySearchClassName])}>
                {emptySearchMessage->Option.getWithDefault(
                  <FormattedMessage defaultMessage="Aucun résultat pour cette recherche" />,
                )}
              </p>
              {additionnalOption->Option.getWithDefault(React.null)}
            </div>
          | Ok(suggestions) =>
            <React.Fragment>
              {suggestions
              ->Array.mapWithIndex((i, suggestion) => {
                <HeadlessUi.Combobox.OptionRender
                  \"as"={Node(HeadlessUi__Shared.fragment)}
                  key={`${suggestion.label}-${i->Int.toString}`}
                  value={suggestion}>
                  {({active, selected}) => {
                    <li
                      className={cx([
                        "relative cursor-default select-none py-2 px-4",
                        `autocompleteSuggestion e2e-autocompleteSuggestion-${i->Int.toString}`,
                        active ? "bg-primary-700 text-white" : "bg-white text-neutral-900",
                        selected ? "font-semibold" : "",
                        optionClassName,
                      ])}>
                      {suggestion.label->React.string}
                    </li>
                  }}
                </HeadlessUi.Combobox.OptionRender>
              })
              ->React.array}
              {additionnalOption->Option.getWithDefault(React.null)}
            </React.Fragment>
          }}
        </HeadlessUi.Combobox.Options>
      }}
    </HeadlessUi.Combobox>
    {children->Option.mapWithDefault(React.null, children =>
      children({
        setSearch,
        setSuggestions,
      })
    )}
  </div>
}
