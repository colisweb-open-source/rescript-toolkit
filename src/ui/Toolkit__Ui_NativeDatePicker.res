@react.component
let make = (~onChange, ~value=?, ~id=?, ~disabled=false, ~isInvalid=false, ~className="") =>
  <div className="relative">
    <input
      ?id
      value=?{value->Option.map(d => d->DateFns.formatWithPattern("yyyy-MM-dd"))}
      disabled
      ariaDisabled={disabled}
      onChange={event => {
        let value = ReactEvent.Form.target(event)["value"]
        onChange(Some(Js.Date.fromString(value))->Option.keep(DateFns.isValid))
      }}
      className={cx([
        "h-[38px] border rounded transition-all duration-150 ease-in-out py-2 pl-4 pr-8 appearance-none w-full bg-white text-gray-800 leading-tight focus:outline-none focus:shadow-none focus:border-info-500 disabled:bg-gray-200 disabled:text-gray-700 focus:z30 relative border-gray-300",
        switch isInvalid {
        | true => "border-danger-500 shadow-danger-500"
        | _ => "border-gray-300"
        },
        className,
      ])}
      type_="date"
    />
  </div>
