type variant = [#default | #text | #link | #outline | #pill]

type size = [#xs | #sm | #md | #lg]

type color = [
  | #black
  | #white
  | #gray
  | #success
  | #info
  | #warning
  | #primary
  | #danger
  | #neutral
  | #neutralLight
  | #dynamic
]

@react.component
let make: (
  ~size: size=?,
  ~color: color=?,
  ~variant: variant=?,
  ~isLoading: bool=?,
  ~onClick: ReactEvent.Mouse.t => unit=?,
  ~onBlur: ReactEvent.Focus.t => unit=?,
  ~onFocus: ReactEvent.Focus.t => unit=?,
  ~disabled: bool=?,
  ~tabIndex: int=?,
  ~ariaLabel: string=?,
  ~ariaLabelledby: string=?,
  ~ariaExpanded: bool=?,
  ~autoFocus: bool=?,
  ~id: string=?,
  ~type_: string=?,
  ~className: string=?,
  ~children: React.element=?,
  ~leftIcon: module(ReactIcons.Icon)=?,
  ~rightIcon: module(ReactIcons.Icon)=?,
  ~iconClassName: string=?,
  ~testId: string=?,
  ~confirmationLabel: string=?,
  ~buttonRef: ReactDOM.domRef=?,
) => React.element
