type compare<'value> =
  | Key(string)
  | ValueEquality
  | Function(
      (
        option<HeadlessUi.Listbox.selectOption<'value>>,
        HeadlessUi.Listbox.selectOption<'value>,
      ) => bool,
    )

type options<'value> = array<HeadlessUi.Listbox.selectOption<'value>>

@react.component
let make = (
  ~options: options<'value>,
  ~compare: compare<'value>,
  ~onChange: option<HeadlessUi.Listbox.selectOption<'value> => unit>=?,
  ~placeholder=?,
  ~defaultOption: option<HeadlessUi.Listbox.selectOption<'value>>=?,
  ~isDisabled=?,
  ~value=?,
  ~name=?,
) => {
  <div className="relative">
    <HeadlessUi.Listbox
      value
      by={switch compare {
      | Key(objectKey) => objectKey
      | ValueEquality =>
        (
          (selectedValue, v2) => {
            selectedValue->Option.mapWithDefault(false, selectedOption =>
              selectedOption.value === v2.value
            )
          }
        )->Obj.magic
      | Function(fn) => fn->Obj.magic
      }}
      onChange={value => {
        onChange->Option.forEach(fn => {
          fn({...value, value: value.value->Option.getUnsafe})
        })
      }}
      disabled=?{isDisabled}
      ?name
      defaultValue=?{defaultOption->Obj.magic}>
      {props => {
        <React.Fragment>
          <HeadlessUi.Listbox.Button
            className="relative flex flex-row items-center justify-between w-full cursor-default rounded-md font-semibold bg-white py-2 pl-3 pr-4 text-left border border-gray-300">
            {buttonProps => {
              <React.Fragment>
                <span className="table-cell truncate text-left">
                  {switch (buttonProps.value, placeholder) {
                  | (None, Some(p)) => p->React.string
                  | (Some(selectedOption), _) => selectedOption.label->React.string
                  | _ => React.null
                  }}
                </span>
                <ReactIcons.FaAngleDown size={20} />
              </React.Fragment>
            }}
          </HeadlessUi.Listbox.Button>
          <HeadlessUi.Transition
            show={props.isOpen}
            className="relative z-20"
            enter="transition duration-100 ease-out"
            enterFrom="transform scale-95 opacity-0"
            enterTo="transform scale-100 opacity-100"
            leave="transition duration-75 ease-out"
            leaveFrom="transform scale-100 opacity-100"
            leaveTo="transform scale-95 opacity-0">
            <HeadlessUi.Listbox.Options
              static={true}
              className="!absolute min-w-[250px] mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm z-20">
              {options
              ->Array.mapWithIndex((i, option) => {
                <HeadlessUi.Listbox.Option
                  key={`listbox-${i->Int.toString}`} value={option} disabled=?{option.disabled}>
                  {props => {
                    <div
                      className={cx([
                        "relative cursor-default select-none py-2 pl-4 pr-4 text-gray-900 flex flex-row items-center gap-2",
                        props.active ? "bg-primary-50" : "bg-white",
                        switch option.disabled {
                        | Some(true) => "text-gray-500"
                        | _ => "text-gray-900"
                        },
                      ])}>
                      {props.selected
                        ? <div
                            className={cx([
                              "w-6 h-6 rounded-full border  inline-flex items-center justify-center flex-shrink-0",
                              switch option.disabled {
                              | Some(true) => "bg-gray-100 border-gray-200"
                              | _ => "bg-white border-gray-500"
                              },
                            ])}>
                            <div className="w-4 h-4 rounded-full bg-primary-700" />
                          </div>
                        : <div
                            className={cx([
                              "w-6 h-6 rounded-full border border-gray-500 bg-white flex-shrink-0",
                              switch option.disabled {
                              | Some(true) => "border-gray-200"
                              | _ => "bg-white border-gray-500"
                              },
                            ])}
                          />}
                      {option.label->React.string}
                    </div>
                  }}
                </HeadlessUi.Listbox.Option>
              })
              ->React.array}
            </HeadlessUi.Listbox.Options>
          </HeadlessUi.Transition>
        </React.Fragment>
      }}
    </HeadlessUi.Listbox>
  </div>
}
