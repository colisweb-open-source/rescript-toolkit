open ReactIntl

module Msg = {
  @@intl.messages
  let copied = {
    id: "clipboard.copied",
    defaultMessage: "Copied to clipboard",
  }
}

@react.component
let make = (~children, ~textToCopy, ~className="", ~intl) => {
  let ref1Clipboard = Toolkit__Hooks.useClipboard(
    textToCopy,
    ~onCopyNotificationMessage=intl->Intl.formatMessage(Msg.copied),
  )

  <div className={cx(["rounded-md border border-info-500 bg-info-50", className])}>
    <div className="relative w-full">
      <div
        className="absolute left-full transform -translate-x-1/2 top-1 p-1 bg-info-50 text-info-500 cursor-pointer hover:bg-info-400 hover:text-white rounded active:bg-info-500"
        onClick={_ => {
          ref1Clipboard.copy()
        }}>
        <ReactIcons.MdContentCopy size=14 />
      </div>
      <div className="py-1 px-2 pr-4"> {children} </div>
    </div>
  </div>
}
