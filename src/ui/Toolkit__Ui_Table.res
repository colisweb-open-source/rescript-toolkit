module Pagination = {
  @react.component
  let make = (~table: ReactTable.instance<'a>, ~className="") =>
    <div className={cx([className, "flex items-center"])}>
      <Toolkit__Ui_Button
        type_="button"
        className="rounded-r-none"
        onClick={_ => table.previousPage()}
        disabled={!table.canPreviousPage}>
        {"<"->React.string}
      </Toolkit__Ui_Button>
      <Toolkit__Ui_Button
        type_="button"
        className="rounded-l-none"
        onClick={_ => table.nextPage()}
        disabled={!table.canNextPage}>
        {">"->React.string}
      </Toolkit__Ui_Button>
      <div className="mx-6">
        {((table.state.pageIndex + 1)->Int.toString ++
          (" / " ++
          {
            let pageCount = table.pageOptions->Array.length
            pageCount === 0 ? 1 : pageCount
          }->Int.toString))->React.string}
      </div>
      <div className="w-20">
        <Toolkit__Ui_Select
          value={table.state.pageSize->Int.toString}
          onChange={value => {
            value
            ->Int.fromString
            ->Option.forEach(pageSize => {
              table.setPageSize(pageSize)
            })
          }}
          options={[25, 50, 100]->Array.map((pageSize): Toolkit__Ui_Select.selectOption => {
            label: pageSize->Int.toString,
            value: pageSize->Int.toString,
          })}
        />
      </div>
    </div>
}

module Core = {
  @react.component
  let make = (
    ~table: ReactTable.instance<'a>,
    ~className="",
    ~emptyMessage,
    ~additionalRow: option<(ReactTable.instanceRow<'a>, 'a) => React.element>=?,
    ~trClassName="",
    ~tdClassName="",
  ) =>
    <div className={cx([className, "flex flex-auto w-full overflow-x-auto relative"])}>
      <Toolkit__Ui_Spread props={table.getTableProps()}>
        <table className="border-b border-gray-300 w-full h-full">
          <thead className="block">
            <tr className="flex">
              {table.headers
              ->Array.mapWithIndex((index, column) => {
                column.isVisible
                  ? <Toolkit__Ui_Spread
                      key={"thead-th" ++ index->Int.toString} props={column.getHeaderProps()}>
                      <td
                        className={cx([
                          tdClassName,
                          "px-2 pt-2 border-b border-r border-gray-300 xl:min-h-[5rem]",
                        ])}
                        key={"thead-thd" ++ index->Int.toString}>
                        <Toolkit__Ui_Spread props={column.getSortByToggleProps()}>
                          <div
                            className="font-semibold text-sm mb-1 text-primary-800 pr-2 relative">
                            {column.render("header", ())}
                            {column.isSorted
                              ? column.isSortedDesc->Option.mapWithDefault(React.null, v =>
                                  <span className="absolute right-0 top-0 mt-1">
                                    {v
                                      ? <ReactIcons.MdKeyboardArrowDown size=18 />
                                      : <ReactIcons.MdKeyboardArrowUp size=18 />}
                                  </span>
                                )
                              : React.null}
                          </div>
                        </Toolkit__Ui_Spread>
                        <Toolkit__Ui_Spread props={column.getResizerProps()}>
                          <div className={"resizer" ++ (column.isResizing ? " isResizing" : "")} />
                        </Toolkit__Ui_Spread>
                        {column.render("filterRender", ())
                        ->Obj.magic
                        ->Option.getWithDefault(React.null)}
                      </td>
                    </Toolkit__Ui_Spread>
                  : React.null
              })
              ->React.array}
            </tr>
          </thead>
          <tbody className="block relative">
            {switch table.page {
            | [] =>
              <tr>
                <td className="p-4 cw-table-noResult"> {emptyMessage->React.string} </td>
              </tr>
            | page =>
              page
              ->Array.mapWithIndex((index, row) => {
                let (_, data) = row.original->Obj.magic
                table.prepareRow(row)

                <React.Fragment key={"tr" ++ index->Int.toString}>
                  <Toolkit__Ui_Spread props={row.getRowProps()}>
                    <tr className={cx([trClassName, "block even:bg-gray-200 min-h-[5.5rem]"])}>
                      {row.cells
                      ->Array.mapWithIndex((index, cell) =>
                        <Toolkit__Ui_Spread
                          key={"spread-td" ++ index->Int.toString} props={cell.getCellProps()}>
                          <td key={"td" ++ index->Int.toString} className="px-4 py-2">
                            {cell.render("cell", ())}
                          </td>
                        </Toolkit__Ui_Spread>
                      )
                      ->React.array}
                    </tr>
                  </Toolkit__Ui_Spread>
                  {additionalRow->Option.mapWithDefault(React.null, cb => cb(row, data))}
                </React.Fragment>
              })
              ->React.array
            }}
          </tbody>
        </table>
      </Toolkit__Ui_Spread>
    </div>
}

module Filters = {
  module Text = {
    @react.component
    let make = (~setFilter, ~defaultValue, ~id, ~placeholder) =>
      <div>
        <Toolkit__Ui_TextInput
          id
          value={defaultValue->Option.getWithDefault("")}
          placeholder
          onChange={event => {
            let value = (event->ReactEvent.Form.target)["value"]
            switch value {
            | "" => setFilter(None)
            | value => setFilter(Some(value))
            }
          }}
        />
      </div>
  }
}
