# Hooks

Some React hooks implemented in Reason, some of these come from [react-use](https://github.com/streamich/react-use).

## API

- `useIsFirstMount`
- `useUpdateEffect`
- `useIsMounted`
- `useTimeoutFn`
- `useUpdate`
- `useDebounce`
- `useGetSet`
- `usePrevious`
- `useInitialPrevious`
- `usePreviousDistinct`
- `useRequest`

## Usage

```rescript
open Toolkit;

module API = {
  module FetchUsers = {
    module Config = {
      type argument = unit;

      [@spice]
      type response = array(user)
      [@spice]
      and user = {
        name: string
      };

      type error = string;
    };

    module Request = Toolkit.Request.Make(Config);
  };
};

[@react.component]
let make = () => {
  let (requestState, execRequest, cancelRequest) = Hooks.useRequest(() => {
    API.FetchUsers.Request.exec()
  }, [||]);

  switch (requestState) {
    | NotAsked => React.null
    | Loading => "loading"->React.string
    | Done(Ok(users)) =>
        users
        ->Array.map(u => u.name->React.string)
        ->React.array
    | Done(Error(err)) => err->React.string
  }
};
```
