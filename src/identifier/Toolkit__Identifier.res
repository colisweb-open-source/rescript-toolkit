module MakeString = () => {
  module Id = {
    @spice
    type t = string
    module Dict = {
      include Js.Dict

      let deleteKey: (t<'a>, string) => t<'a> = %raw("function (dict, key) {
        const newDict = Object.assign({},dict);
        delete newDict[key];
        return newDict;
      }")

      let t_encode = (encoder, dict): Js.Json.t => Js.Json.Object(Js.Dict.map(encoder, dict))

      let t_decode = (decoder, json) => {
        open Spice
        switch (json: Js.Json.t) {
        | Js.Json.Object(dict) =>
          dict
          ->Js.Dict.entries
          ->Belt.Array.reduce(Ok(Js.Dict.empty()), (acc, (key, value)) =>
            switch (acc, decoder(value)) {
            | (Error(_), _) => acc

            | (_, Error({path} as error)) => Error({...error, path: "." ++ (key ++ path)})

            | (Ok(prev), Ok(newVal)) =>
              let () = prev->Js.Dict.set(key, newVal)
              Ok(prev)
            }
          )
        | _ => Error({path: "", message: "Not a dict", value: json})
        }
      }
    }
  }

  @spice
  type t = Id.t

  module Dict = Id.Dict

  external make: string => t = "%identity"
  external toString: t => string = "%identity"
}

module MakeInt = () => {
  module Id: {
    @spice
    type t
  } = {
    @spice
    type t = int
  }

  @spice
  type t = Id.t
  external make: int => t = "%identity"
  external toInt: t => int = "%identity"
  external magic: 'a => t = "%identity"

  let toString = t => t->toInt->Int.toString
  let castAsString = t => t->toInt->Int.toString->magic
}

module MakeStringOrInt = () => {
  module Id = {
    let encoder: Spice.encoder<string> = id => id->Spice.stringToJson

    let decoder: Spice.decoder<string> = json =>
      switch (Spice.stringFromJson(json), Spice.intFromJson(json)) {
      | (Ok(v), _) => Ok(v)
      | (_, Ok(v)) => Ok(v->Int.toString)
      | (Error(err), _) => Error(err)
      }

    let codec: Spice.codec<string> = (encoder, decoder)

    @spice
    type t = @spice.codec(codec) string
    module Dict = {
      include Js.Dict

      let deleteKey: (t<'a>, string) => t<'a> = %raw("function (dict, key) {
  const newDict = Object.assign({},dict);
  delete newDict[key];
  return newDict;
}")

      let t_encode = (encoder, dict) => dict->Js.Dict.map(encoder, _)->Js.Json.object_

      let t_decode = (decoder, json) => {
        open Spice
        switch Js.Json.decodeObject(json) {
        | Some(dict) =>
          dict
          ->Js.Dict.entries
          ->Belt.Array.reduce(Ok(Js.Dict.empty()), (acc, (key, value)) =>
            switch (acc, decoder(value)) {
            | (Error(_), _) => acc

            | (_, Error({path} as error)) => Error({...error, path: "." ++ key ++ path})

            | (Ok(prev), Ok(newVal)) =>
              let () = prev->Js.Dict.set(key, newVal)
              Ok(prev)
            }
          )
        | None => Error({path: "", message: "Not a dict", value: json})
        }
      }
    }
  }

  @spice
  type t = Id.t

  module Dict = Id.Dict

  external make: string => t = "%identity"
  external toString: t => string = "%identity"
}
