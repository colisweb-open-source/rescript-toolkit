module Array = {
  include Belt.Array

  let findMap: (array<'a>, 'a => option<'b>) => option<'b> = (array, findMapper) => {
    let result = ref(None)
    let index = ref(0)

    while result.contents->Option.isNone && index.contents < array->Array.length {
      result := findMapper(array[index.contents]->Option.getExn)
      index := index.contents + 1
    }

    result.contents
  }

  let findMapWithIndex: (array<'a>, (int, 'a) => option<'b>) => option<'b> = (
    array,
    findMapper,
  ) => {
    let result = ref(None)
    let index = ref(0)

    while result.contents->Option.isNone && index.contents < array->Array.length {
      result := findMapper(index.contents, array[index.contents]->Option.getExn)
      index := index.contents + 1
    }

    result.contents
  }
}
